import numpy as np
from lib import pycrf
import itertools

class CRFImageDenoiser3D:


  #Create the CRFImageDenoiser3D object using the 3D noisy image array as the input argument
  # Initializes the CRF and sets initial labels
  def __init__(self, noisyImageArray):
    self.noisyImageArray = noisyImageArray
    self.numPixels = self.noisyImageArray.shape[0] * self.noisyImageArray.shape[1] * self.noisyImageArray.shape[2]
    self.numUniqueLabels = len(np.unique(self.noisyImageArray))
    self.crf = pycrf.CRF(self.numPixels, self.numUniqueLabels)
    self.crf.setLabels(self.noisyImageArray.flatten().astype('int32'))
    self.r = [-1,0, 1]
    self.penaltyWeight = 200
    print "CRF initialized"
  
  def index(self, i, j, k):
      return k + self.noisyImageArray.shape[1] * j + self.noisyImageArray.shape[2] * i



  def setDataCost(self):
    datacost = np.zeros(self.numPixels * self.numUniqueLabels,dtype='float32')
    for p in xrange(self.numPixels):
       for l in xrange(self.numUniqueLabels):
          datacost[p*self.numUniqueLabels+l] = (self.noisyImageArray.flat[p]-l)**2
    self.crf.setDataCost(datacost)
    print "Calculated data cost for the CRF"

  def setLabelCost(self):
    smoothcost = np.zeros(self.numUniqueLabels * self.numUniqueLabels,dtype='float32')
    for l1 in xrange(self.numUniqueLabels):
      for l2 in xrange(self.numUniqueLabels):
        smoothcost[l1+self.numUniqueLabels*l2] = float(l1 != l2)*self.penaltyWeight
    self.crf.setSmoothCost(smoothcost)
    print "Calculated label cost for the CRF"

  def findNeighborhood(self):
    r = self.r
    self.neighbourhood = []
    for a,b,c in itertools.product(r,r,r):
      if abs(a)+abs(b)+abs(c) > 0:
        self.neighbourhood.append((a,b,c))
    print self.neighbourhood
    print "Neighbourhood calculated"

  def buildCRFGraph(self):
    for i in xrange(self.noisyImageArray.shape[0]):
      for j in xrange(self.noisyImageArray.shape[1]):
        for k in xrange(self.noisyImageArray.shape[2]):
          for a,b,c in self.neighbourhood:
            if (0 <= i+a < self.noisyImageArray.shape[0]
                 and 0 <= j+b < self.noisyImageArray.shape[1]
                 and 0 <= k+c < self.noisyImageArray.shape[2]
                 and self.index(i,j,k) < self.index(i+a,j+b,k+c)):
              self.crf.setNeighbors(self.index(i,j,k), self.index(i+a,j+b,k+c), 1)
    print "Completed building the CRF graph"

  def executeCRF(self):
    self.setDataCost()
    self.setLabelCost()
    self.findNeighborhood()
    self.buildCRFGraph()
    print "initial energy: ", self.crf.compute_energy()
    self.crf.expansion()
    print "final energy: ", self.crf.compute_energy()
    denoisedImageArray = np.reshape(self.crf.getLabels(), self.noisyImageArray.shape)
    return denoisedImageArray
