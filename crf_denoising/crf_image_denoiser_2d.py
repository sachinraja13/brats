import numpy as np
from lib import pycrf
import itertools
import sys

BIAS_VALUE = 100

class CRFImageDenoiser2D:


  #Create the CRFImageDenoiser object using the 3D noisy image array as the input argument
  # Initializes the CRF and sets initial labels
  def __init__(self, noisyImageArray):
    self.noisyImageArray = noisyImageArray
    self.initializeCRF()

  def prepareCorrespondingLabelArray(self, labelArray):
    self.processedLabelArray = np.array(labelArray)
    for i in range(labelArray.shape[0]):
      self.processedLabelArray[i] = self.uniqueLabelsDict[labelArray[i]]

  def initializeCRF(self):
    self.numPixels = self.noisyImageArray.shape[0] * self.noisyImageArray.shape[1]
    self.uniqueLabels = np.unique(self.noisyImageArray)
    self.uniqueLabelsDict = {}
    i = 0
    for lab in self.uniqueLabels:
      self.uniqueLabelsDict[lab] = i
      i = i + 1
    self.numUniqueLabels = len(self.uniqueLabels)
    if self.numUniqueLabels < 5:
      return False
    self.crf = pycrf.CRF(self.numPixels, self.numUniqueLabels)
    self.prepareCorrespondingLabelArray(self.noisyImageArray.flatten().astype('int32'))
    self.crf.setLabels(self.processedLabelArray)
    self.r = [-1,0, 1]
    self.penaltyWeight = 2
    print "CRF initialized"
    return True
  
  def index(self, i, j):
      return j + self.noisyImageArray.shape[1] * i



  def setDataCost(self):
    datacost = np.zeros(self.numPixels * self.numUniqueLabels,dtype='float32')
    for p in xrange(self.numPixels):
      for l in xrange(self.numUniqueLabels):
        datacost[p*self.numUniqueLabels+l] = (self.processedLabelArray[p]- l)**2
    self.crf.setDataCost(datacost)
    print "Calculated data cost for the CRF"

  def setLabelCost(self):
    smoothcost = np.zeros(self.numUniqueLabels * self.numUniqueLabels,dtype='float32')
    for l1 in xrange(self.numUniqueLabels):
      for l2 in xrange(self.numUniqueLabels):
        smoothcost[l1+self.numUniqueLabels*l2] = float(l1 != l2)*self.penaltyWeight
    self.crf.setSmoothCost(smoothcost)
    print "Calculated label cost for the CRF"

  def findNeighborhood(self):
    r = self.r
    self.neighbourhood = []
    for a,b in itertools.product(r,r):
      if abs(a)+abs(b) > 0:
        self.neighbourhood.append((a,b))
    print "Neighbourhood calculated"

  def buildCRFGraph(self):
    for i in xrange(self.noisyImageArray.shape[0]):
      for j in xrange(self.noisyImageArray.shape[1]):
        for a,b in self.neighbourhood:
          if (0 <= i+a < self.noisyImageArray.shape[0]
               and 0 <= j+b < self.noisyImageArray.shape[1]
                and self.index(i,j) < self.index(i+a,j+b)):
            self.crf.setNeighbors(self.index(i,j), self.index(i+a,j+b), 1)
    print "Completed building the CRF graph"

  def executeCRF(self):
    if not self.initializeCRF():
      return self.noisyImageArray
    self.setDataCost()
    self.setLabelCost()
    self.findNeighborhood()
    self.buildCRFGraph()
    print "initial energy: ", self.crf.compute_energy()
    self.crf.expansion()
    print "final energy: ", self.crf.compute_energy()
    denoisedImageArray = np.reshape(self.crf.getLabels(), self.noisyImageArray.shape)
    for x in range(denoisedImageArray.shape[0]):
      for y in range(denoisedImageArray.shape[1]):
        index = denoisedImageArray[x,y]
        denoisedImageArray[x,y] = self.uniqueLabels[index]
    return denoisedImageArray

