import sys, io
from argparse import ArgumentParser

class DefaultHelpParser(ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

if __name__ == '__main__':
    parser = DefaultHelpParser()
    parser.add_argument('configFile', help="path of model's config file")
    parser.add_argument('--train', help='provide this argument to train model', action='store_true')
    parser.add_argument('--predict', help='provide this argument to predict using trained model', action='store_true')
    parser.add_argument('--postproc', help='provide this argument to post-process (denoise) predictions already made', action='store_true')
    parser.add_argument('--genOverlay', help='provide this argument to generate and save a visualization of predictions against ground truth', action='store_true')
    parser.add_argument('--overlayInputPostProcessed', help='provide this argument to to use post-processed predictions as input', action='store_true')
    parser.add_argument('--trainingSteps', help='Number of steps for training. Provide when trainOrPredict is set to train in config file. Default=1', default=1, type=int)
    args = parser.parse_args()
    if not (args.train or args.predict or args.postproc or args.genOverlay):
        parser.error('Invalid usage! Must provide a task.')

    import numpy as np
    from generic.mha_image import MhaImage
    from generic.patient_data import PatientData
    from generic.input_generator import InputGenerator
    from executors.glioma_segmentor import GliomaSegmentor
    from executors.glioma_segmentor_trainer import GliomaSegmentorTrainer
    from executors.prediction_post_processor import PredictionPostProcessor
    from executors.output_visualizer import OutputVisualizer


    if args.train:
        gliomaSegmentorTrainer = GliomaSegmentorTrainer(args.configFile)
        gliomaSegmentorTrainer.executeTrainingAlgorithm(args.trainingSteps)
    if args.predict:
        gliomaSegmentor = GliomaSegmentor(args.configFile)
        gliomaSegmentor.predictGliomaRegions()
    if args.postproc:
        predictionPostProcessor = PredictionPostProcessor(args.configFile)
        predictionPostProcessor.postProcessPredictions()
    if args.genOverlay:
        outputVisualizer = OutputVisualizer(args.configFile)
        outputVisualizer.prepareVisualizations(postProcessedPredictions=args.overlayInputPostProcessed)
