import caffe
import numpy as np
import sys, io
import os

CPU_OR_GPU = 'GPU'

class BaseCaffeModel(object):
        
        def __init__(self, trainingModelParams):
                '''Initializes the caffe learning model'''
                
                # Instance variables:
                #
                # trainingModelParams - dictionary loaded from config.json file
                # modelName: name of the caffe model file to be saved (modelName_<number of iterations>.caffemodel)
                # modelDir: Directory of the model where all the prototxt files, extending python module, config file, predictions
                # and saved parameters exist relative to the brats root directory.
                # trainOrPredict: string 'train' or 'predict'. Loads solver-prototxt for traing and deploy-net-prototxt for making predictions
                # solverPrototxtFilePath: solver.prototxt file path relative to the model directory (modelDir)
                # predictPrototxtFilePath: deploy-prototxt file path relative to the model directory (modelDir)
                # trainTestPrototxtFilePath: train-test-prototxt file path relative to the model directory (modelDir)
                # cascadedArchitecture: boolean. True if cascaded architecture is going to be used where base networks are already trained
                # cascadedNetworksFolder: path for folder containing prototxt and caffemodel files for the base networks,
                #                         '' if cascadedArchitecture is False
                # cascadedDeployNetPrototxts: prototxt.deploy files for trained base networks of the cascaded architecture,
                #                             [] if cascadedArchitecture is False
                # cascadedNetsModelFiles: caffemodel files for trained base networks of the cascaded architecture,
                #                         [] if cascadedArchitecture is False
                # cascadedClassifiers: caffe Classifier objects for trained base networks,
                #                         [] if cascadedArchitecture is False
                # classifier: caffe Classifier object when trainOrPredict is 'predict'

                self.modelName = trainingModelParams['modelName']
                self.modelDir = trainingModelParams['modelDir']
                self.learntModelsDir = trainingModelParams['learntModelsDir']
                self.cascadedArchitecture = trainingModelParams['cascadedArchitecture']
                self.cascadedNetworksFolder = trainingModelParams['cascadedNetworksFolder']
                self.cascadedDeployNetPrototxts = trainingModelParams['cascadedDeployNetPrototxts']
                self.cascadedNetsModelFiles = trainingModelParams['cascadedNetsModelFiles']
                if self.modelDir is not None and not os.path.exists(self.modelDir):
                        os.makedirs(self.modelDir)
                if self.cascadedNetworksFolder is not None and os.path.exists(str(self.modelDir) + '/' + str(self.cascadedNetworksFolder)):
                        os.makedirs(str(self.modelDir) + '/' + str(self.cascadedNetworksFolder))
                if self.learntModelsDir is not None and not os.path.exists(self.modelDir + '/' + self.learntModelsDir):
                        os.makedirs(self.modelDir + '/' + self.learntModelsDir)
                self.trainOrPredict = trainingModelParams['trainOrPredict']
                self.solverPrototxtFilePath = str(self.modelDir) + '/' + str(trainingModelParams['solverPrototxtFile'])
                self.predictPrototxtFilePath = str(self.modelDir) + '/' + str(trainingModelParams['predictPrototxtFile'])
                self.trainTestPrototxtFilePath = str(self.modelDir) + '/' + str(trainingModelParams['trainTestPrototxtFile'])
                if self.cascadedArchitecture:
                        #Initialize all cascaded convolutional neural networks with corresponding model files. PatchSizes array should be corresponding to the deploy.net.prototxts. Only input cascaded architecture is supported using which best results have been observed. Outputs from all models is depth wise appended to the input.
                        self.cascadedClassifiers = []
                        for i in range(len(cascadedDeployNetPrototxts)):
                                cascadedDeployNetPrototxt = str(self.modelDir) + '/' + str(self.cascadedNetworksFolder) + '/' + str(cascadedDeployNetPrototxts[i])
                                cascadedNetModelFile = str(self.modelDir) + '/' + str(self.cascadedNetworksFolder) + '/' + str(cascadedNetsModelFiles[i])
                                cascadedClassifier = caffe.Classifier(cascadedDeployNetPrototxt, cascadedNetModelFile)
                                self.cascadedClassifiers.append(cascadedClassifier)
                if 'train' in self.trainOrPredict:
                        #if command is 'train', create an instance of caffe's SGDSolver with solverPrototxtFilePath
                        self.solver = caffe.SGDSolver(self.solverPrototxtFilePath)
                else:
                        #if command is to 'predict', set caffe mode to GPU and read caffe model from file using which to predict
                        # function to read model from file needs to be called explicitly
                        self.classifier = None
                        caffe.set_mode_gpu()

                print "Caffe network initiation completed"

        def convertToAppropriateShape(self, xs, ys):
                '''Converts cascaded input patches to shapes and data types as expected expected by caffe.'''
                
                # Arguments:
                #
                # 1. xs - list of 4D numpy arrays [NumBatches, each element of corresponding per input shape] ([B,X,Y,C]) for different patch sizes and per input shapes. The list should be in the non-increasing order of the patch sizes for the cascaded architecture to function, else an exception would be thrown.
                # 2. ys - numpy array of (batchSize, number of unique labels) - one hot encoded labels for all the input patches. Can be None if input is for prediction.
                #
                # Output:
                #
                # 1. xs - list of 4D numpy arrays swapped as [B,C,X,Y] (as expected by caffe) for each input patchsize and corresponding per input shape
                # 2. ys - 4D numpy array [Batch Size, 1, 1, 1]. Labels are integer labels
                
                if ys is not None:
                        y = self.getIntegerLabelListFromOneHotTensor(ys)
                        y = np.array(y, dtype=np.float32)
                        batchSize = y.shape[0]
                        y = y.reshape((batchSize, 1, 1, 1))
                else:
                        y = None
                convertedXS = []
                for x in xs:
                        np_x = np.array(x, dtype=np.float32)
                        if len(np_x.shape) == 4:
                                convertedX = np.swapaxes(np_x, 2, 3)
                                convertedX = np.swapaxes(convertedX, 1, 2)
                        else:
                                convertedX = np_x
                        convertedXS.append(convertedX)

                return (convertedXS, y)

        def getOutputsFromOtherConvNetsForInputCascade(self, xs):
                '''Gets predictions form the cascaded convolutional networks in non-increasing order of patch sizes.

                There is just one inout, data0 and name of output convolution/max-pooling layer is cascadeOutput.
                Note: Dimensions of last input (xs[len(xs)-1]) should be same as dimensions of
                cascadeOutput for input concatenation along 2nd dimension (along channels).
                '''

                predictions = []
                for i in range(len(xs)):
                        x = xs[i]
                        cascadedClassifier = self.cascadedClassifiers[i]
                        cascadedClassifier.blobs['data0'].data[...] = x
                        batchPredictions = cascadedClassifier.forward(blobs=['prob', 'cascadeoutput'])['cascadeoutput']
                        predictions.append(batchPredictions)
                return predictions
                        
        def getInputPatchesForCascadedArchitectures(self, xs):
                '''Generates final input for cascaded architectures, which would be the inputs with smallest patch size concatenated with cascaded output predictions of all others along channels.
                
                This method should be called after calling convertToAppropriateShape.
                '''

                predictions = self.getOutputsFromOtherConvNetsForInputCascade(xs[:len(xs)-1])
                inputBatch = xs[len(xs) - 1]
                #Append predictions of cascaded nets with the input batch
                cascadedInputBatch = np.concatenate(predictions, axis=1)
                return cascadedInputBatch

        def prepareInputForTrainStep(self, train_xs, train_y):
                '''Feeds data to the caffe train-test network during training for each of the patch sizes and corresponding per input shapes.

                Calls convertToAppropriateShape before feeding.
                '''

                train_xs, train_y = self.convertToAppropriateShape(train_xs, train_y)
                if not self.cascadedArchitecture:
                        for i in range(len(train_xs)):
                                self.solver.net.blobs['data'+str(i)].data[...] = train_xs[i]
                        self.solver.net.blobs['label'].data[...] = train_y
                else:
                        cascadedInputBatch = self.getInputPatchesForCascadedArchitectures(train_xs)
                        self.solver.net.blobs['data0'].data[...] = cascadedInputBatch 
                        self.solver.net.blobs['label'].data[...] = train_y

        def prepareInputForTestStep(self, test_xs, test_y=None):
                '''Feeds data to the caffe train-test network during test(validation) for each of the patch sizes and corresponding per input shapes.

                Calls convertToAppropriateShape before feeding. If labels are set to None, labels blob in initialized as a 4d array of (Batch Size, 1,1,1) with all 0s.
                '''
                
                test_xs, test_y = self.convertToAppropriateShape(test_xs, test_y)
                if not self.cascadedArchitecture:
                        for i in range(len(test_xs)):
                                self.solver.test_nets[0].blobs['data'+str(i)].data[...] = test_xs[i]
                        self.solver.test_nets[0].blobs['label'].data[...] = test_y
                else:
                        cascadedInputBatch = self.getInputPatchesForCascadedArchitectures(test_xs)
                        self.solver.test_nets[0].blobs['data0'].data[...] = cascadedInputBatch
                        self.solver.test_nets[0].blobs['label'].data[...] = test_y

        def prepareInputForPredictions(self, xs):
                '''Feeds data to the caffe deploy network during test(validation) for each of the patch sizes and corresponding per input shapes.

                Calls convertToAppropriateShape before feeding. If labels are not set, labels blob in initialized as a 4d array of (Batch Size, 1,1,1) with all 0s.
                '''
                
                xs, y = self.convertToAppropriateShape(xs, None)
                if not self.cascadedArchitecture:
                        for i in range(len(xs)):
                                self.classifier.blobs['data'+str(i)].data[...] = xs[i]
                else:
                        cascadedInputBatch = self.getInputPatchesForCascadedArchitectures(xs)
                        self.classifier.blobs['data0'].data[...] = cascadedInputBatch

        def executeTrainStep(self, train_xs, train_y, keep_prob_train_step, stepNumber):
                '''Feeds data to the caffe network and executes one step of caffe's SGDSolver.'''

                self.prepareInputForTrainStep(train_xs, train_y)
                self.solver.step(1)

        def printTestAccuracy(self, test_xs, test_y):
                '''Calculates accuracy on the test (validation) batch.'''

                self.prepareInputForTestStep(test_xs, test_y)
                test_accuracy = self.solver.test_nets[0].forward(blobs=['accuracy'])['accuracy']
                print("\ntest accuracy: " + str(test_accuracy))

        def printTestLoss(self, test_xs, test_y):
                '''Calculates loss on the test (validation) batch.'''

                self.prepareInputForTestStep(test_xs, test_y)
                test_loss = self.solver.test_nets[0].forward(blobs=['loss'])['loss']
                print("\ntest loss: " + str(test_loss))

        def getModelFileName(self, iteration):
                '''Gets filename using which the model parameters are saved on the disk.'''

                modelFileName = self.modelName + '_' + str(iteration) + '.caffemodel'
                return modelFileName

        def saveParameters(self, saverObj, modelFileName):
                '''Saves caffe's parameters to the disk in the base folder relative to the working model directory.'''

                modelFilePath = str(self.modelDir) + '/' + str(self.learntModelsDir) + '/' + (modelFileName)
                modelFilePath = str(modelFilePath)
                print modelFilePath
                self.solver.net.save(modelFilePath)
                print("Model saved : " + str(modelFilePath))

        def loadParameters(self, saverObj, modelFileName):
                '''Loads model parameters from caffemodel file to train-test network for resuming training and to deploy prototxt file for making predictions.'''
                
                if modelFileName is None:
                        return None
                modelFilePath = str(self.modelDir) + '/' + str(self.learntModelsDir) + '/' + str(modelFileName)
                modelFilePath = str(modelFilePath)
                if 'train' in self.trainOrPredict:
                        self.solver.net.copy_from(modelFilePath)
                        self.solver.test_nets[0].copy_from(modelFilePath)
                else:
                        self.classifier = caffe.Classifier(self.predictPrototxtFilePath, modelFilePath)
                print("Model restored : " + str(modelFilePath))

        def getPredictions(self, xs):
                '''Get predictions using batch of only input patches.

                Use train-test network if trainOrPredict is set to 'train' or deploy network if trainOrPredict is set to 'predict'.
                '''
                
                if 'train' in self.trainOrPredict:
                        self.prepareInputForTestStep(xs)
                        predictions = self.solver.test_nets[0].forward(blobs=['prob'])['prob']
                else:
                        self.prepareInputForPredictions(xs)
                        predictions = self.classifier.forward(blobs=['prob'])['prob']
                return predictions

        def getSaverObject(self):
                '''Returns saver object. Not useful with caffe..'''

                return None

        def getIntegerLabelListFromOneHotTensor(self, y):
                '''Returns list of labels predicted by the model.

                Converts the probabilities returned by caffe predictions to the integer list of labels using index of max value of
                the probabilities along each row as the int label for that row.
                '''

                return [list(p).index(max(p)) for p in y]
