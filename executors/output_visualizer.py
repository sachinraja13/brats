import sys, io
import os
import json
import numpy as np
from generic.input_generator import InputGenerator
from generic.project_util import ProjectUtil
from generic.mha_image import MhaImage

class OutputVisualizer:

	def __init__(self, configFile):

		# Load config.json file
		self.config = None
		with open(configFile) as jsonConfigFile:
				self.config = json.load(jsonConfigFile)
				
		self.inputGenerator = InputGenerator(self.config['datasetFiles'], None, None, self.config['normalizationParams'], self.config['uniqueLabels'], False, None, None)
		# Create ProjectUtil object
		self.projectUtil = ProjectUtil()

	#Creates visualization mha image files for all patientIDs under predictionIndices key in the config file.
	def prepareVisualizations(self, postProcessedPredictions=True):
		visualizationsDir = str(self.config['trainingModelParams']['modelDir']) + '/' + str(self.config['visualizationsDir'])
		predictionFilesDir = str(self.config['trainingModelParams']['modelDir']) + '/' + str(self.config['predictionFilesDir'])
		if not os.path.exists(visualizationsDir):
			os.makedirs(visualizationsDir)
		for datasetFile in self.config['predictionIndices']:
			for patientIndex in self.config['predictionIndices'][datasetFile]:
				patientData = InputGenerator.patientDataObjectManager.getPatientDataObject(datasetFile, patientIndex)
				print("\nStarting to prepare visualization for : " + patientData.patientID)
				outputFilePath = str(visualizationsDir) + '/' + str(patientData.patientID) + '_overlay.mha'
				trueLabelFilePath = patientData.labelData.imageFilePath
				if postProcessedPredictions and os.path.exists(str(predictionFilesDir) + '/' + str(patientData.patientID) + '_denoised.mha'):
					predictionFilePath = str(predictionFilesDir) + '/' + str(patientData.patientID) + '_denoised.mha'
				else:
					predictionFilePath = str(predictionFilesDir) + '/' + str(patientData.patientID) + '.mha'
				self.projectUtil.generateOverlayImage(trueLabelFilePath, predictionFilePath, outputFilePath)
				print("Completed preparing visualization for : " + patientData.patientID)

		print("\n\nColor Mapping:")
		print("Red -> White(4)")
		print("Light Blue -> Lightest Grey(3)")
		print("Dark Blue -> Next Lightest Grey(2)")
		print("Green -> Darkest Grey(1)")
		print("Black -> Black(0)")