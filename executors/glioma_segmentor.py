import sys, io
import os
import json
import pkgutil
import Queue
from threading import Thread
import numpy as np
from generic.input_generator import InputGenerator
from generic.multi_threaded_patient_prediction_batch_generator import MultiThreadedPatientPredictionBatchGenerator
from generic.project_util import ProjectUtil
from generic.mha_image import MhaImage
import time

class GliomaSegmentor:

        def __init__(self, configFile):
                self.exitSignal = False

                # Load config.json file
                self.config = None
                with open(configFile) as jsonConfigFile:
                        self.config = json.load(jsonConfigFile)
                
                # Create training model object
                # Load class based on model folder name and python architecture file name
                # Create object of the loaded class with the trainingModelParams read from config file
                impImporterInstance = list(pkgutil.iter_modules([self.config['trainingModelParams']['modelDir']]))[0][0]
                TrainingModel = impImporterInstance.find_module(self.config['moduleName']).load_module(self.config['moduleName']).TrainingModel
                self.trainingModel = TrainingModel(self.config['trainingModelParams'])
                self.trainingModel.loadParameters(self.trainingModel.getSaverObject(), self.config['parametersFile'])
                
                # Create ProjectUtil object
                self.projectUtil = ProjectUtil()

        def createInputGeneratorObject(self):
                '''Creates an instance of InputGenerator class.
                
                Arguments required from json config file:
                1. datasetFiles
                2. trainIndices = None
                3. testIndices = None
                4. normalizationParams
                5. uniqueLabels
                6. keepPatientDataObjectsInMemory = False (after prediction, object is no longer required to stay in memory)
                7. denoise - Dict of booleans. Curvature flow algorithm to be executed on what all modalities
                8. n4BiasFieldCorrection - Dict of booleans. N4 Nias Field Correction algorithm to be executed on what all modalities 
                '''
                
                inputGenerator = InputGenerator(self.config['datasetFiles'], None, None, self.config['normalizationParams'], self.config['uniqueLabels'], False, self.config['denoise'], self.config['n4BiasFieldCorrection'])
                return inputGenerator

        def tagImage(self, patientData, outputFilePath):
                '''Create an instance of MultiThreadedPatientPredictionBatchGenerator to generate batches for prediction with arguments from config file.

                Threads for generating prediction batches are started in the constructor of MultiThreadedPatientPredictionBatchGenerator.
                Method to predict labels for all coordinates is called. All coordinates have a label of 0 by default. Not all non zeros are predicted.
                MHA image is generated from the predictedImage array and prediction results are calculated using the true labels image.
                '''

                start = time.time()
                print("starting prediction for " + patientData.patientID)
                predictionBatchGenerationParams = {}
                predictionBatchGenerationParams['patchSizes'] = self.config['patchSizes']
                predictionBatchGenerationParams['perInputShapes'] = self.config['perInputShapes']
                predictionBatchGenerationParams['threeDPatches'] = self.config['threeDPatches']
                predictionBatchGenerationParams['normalized'] = self.config['normalizeBatches']
                patientPredictionBatchGenerator = MultiThreadedPatientPredictionBatchGenerator(patientData, self.config['predictionBatchSize'], predictionBatchGenerationParams, self.config['numPredBatchesGenThreads'], self.config['predCoordinatesQueueMaxSize'], self.config['predBatchesQueueMaxsize'])
                self.predictImageLabels(patientPredictionBatchGenerator)
                self.projectUtil.generateMhaImageFromImageArray(patientPredictionBatchGenerator.patientData.predictedImage, outputFilePath)
                self.calculatePredictionResults(patientData, outputFilePath)
                end = time.time()
                timeTaken = float(end - start) / 60.0
                print("completed prediction for " + patientData.patientID)
                print("Total time taken to segment patient " + patientData.patientID + " : " + str(timeTaken) + " mins")

        def calculatePredictionResults(self, patientData, outputFilePath):
                '''Using predicted image file path and true image file path, MhaImage objects are generated and F-Scores are calculated and displayed.'''
                if patientData.labelImageFilePath is not None:
                        trueLabelImage = MhaImage(None, None, patientData.labelImageFilePath)
                        predLabelImage = MhaImage(None, None, outputFilePath)
                        trueLabelImage.getArrayFromFile(denoise=None)
                        predLabelImage.getArrayFromFile(denoise=None)
                        self.projectUtil.comparePredictedImageWithTrueImage(trueLabelImage.completeImageArray,predLabelImage.completeImageArray, uniqueLabels=self.config['uniqueLabels'], predictionPatientID=patientData.patientID)
                        self.projectUtil.comparePredictedImageWithTrueImageAccuracy(trueLabelImage.completeImageArray,predLabelImage.completeImageArray)

        def most_common(self, lst):
                '''Return the max occuring value in a list.'''
                return max(set(lst), key=lst.count)

        def predictImageLabels(self, patientPredictionBatchGenerator):
                '''Predicts labels of coordinates from input patches across these coordinates (patientPredictionBatchGenerator).
                
                Prediction batch None means all the the elements from predictionBatchesQueue have been processed.
                '''

                batchNo = 1
                while True:
                        predictionBatch = patientPredictionBatchGenerator.getNextPredictionBatch(self.config['queueGetRetryInterval'])
                        if predictionBatch is None:
                                break
                        start = time.time()
                        print("starting to process batch : " + str(batchNo))
                        coordinates = predictionBatch[0]
                        xs = predictionBatch[1]
                        # Predict labels from input patches
                        ys = self.trainingModel.getIntegerLabelListFromOneHotTensor(self.trainingModel.getPredictions(xs))
                        coordinateLabelsDict = {}
                        for i in range(len(coordinates)):
                                y = ys[i]
                                coordinate = coordinates[i]
                                # If coordinate is None, need not predict, input patch was added just to fill batch
                                if coordinate is not None:
                                        # Required for 3d patches where there will be 3 predictions for each coordinate
                                        # one each for patches in xy, xz and yz planes
                                        if coordinate not in coordinateLabelsDict:
                                                coordinateLabelsDict[coordinate] = []
                                        coordinateLabelsDict[coordinate].append(y)

                        for coordinate in coordinateLabelsDict:
                                # Max occuring prediction for a coordinate is voted as the final prediction
                                y = self.most_common(coordinateLabelsDict[coordinate])
                                # patient predicted image array is updated with the final prediction
                                patientPredictionBatchGenerator.patientData.predictedImage[coordinate[0], coordinate[1], coordinate[2]] = y

                        end = time.time()
                        timeTaken = float(end - start) / 60.0
                        print("batch processing complete : " + str(batchNo) + " (" + str(timeTaken) + " mins)")
                        print("size of queue : " + str(patientPredictionBatchGenerator.predictionBatchesQueue.qsize()))
                        batchNo = batchNo + 1

        def predictGliomaRegions(self):
                '''Performs the prediction process for all the indexes specified in the predictionIndices dict in the config file.'''

                predictionFilesDir = str(self.trainingModel.modelDir) + '/' + str(self.config['predictionFilesDir'])
                if not os.path.exists(predictionFilesDir):
                        os.makedirs(predictionFilesDir)
                inputGenerator = self.createInputGeneratorObject()
                for datasetFile in self.config['predictionIndices']:
                        for patientIndex in self.config['predictionIndices'][datasetFile]:
                                patientData = InputGenerator.patientDataObjectManager.getPatientDataObject(datasetFile, patientIndex)
                                outputFilePath = str(predictionFilesDir) + '/' + str(patientData.patientID) + '.mha'
                                self.tagImage(patientData, outputFilePath)
