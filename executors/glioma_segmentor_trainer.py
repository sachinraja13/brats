import sys, io
import json
import pkgutil
import Queue
from threading import Thread
from generic.input_generator import InputGenerator
from generic.project_util import ProjectUtil
import time


class GliomaSegmentorTrainer:

        def __init__(self, configFile):
                # Constructor: Takes in the config file and creates one-time objects

                self.exitSignal = False
                # Load the config.json file
                self.config = None
                with open(configFile) as jsonConfigFile:
                        self.config = json.load(jsonConfigFile)

                # Object of ProjectUtil class
                self.projectUtil = ProjectUtil()

                # Create the trainingModel object by loading appropriate module based on model folder.
                # Each model folder has its own architecture files and a config file.
                # Create object of the loaded class with the trainingModelParams read from config file
                
                impImporterInstance = list(pkgutil.iter_modules([self.config['trainingModelParams']['modelDir']]))[0][0]
                TrainingModel = impImporterInstance.find_module(self.config['moduleName']).load_module(self.config['moduleName']).TrainingModel
                self.trainingModel = TrainingModel(self.config['trainingModelParams'])
                self.trainingModel.loadParameters(self.trainingModel.getSaverObject(), self.config['parametersFile'])

                # Training batch queue based on maxQueueSize
                self.trainBatchQueue = Queue.Queue(maxsize=self.config['maxQueueSize'])
                # Test batch queue based on maxQueueSize
                self.testBatchQueue = Queue.Queue(maxsize=self.config['maxQueueSize'])
                # Create list of training batch generating threads
                self.trainBatchProducingThreads = self.createDataProducingThreads(len(self.config['trainIndices']), self.getTrainBatch)
                # Create list of test batch generating threads
                self.testBatchProducingThreads = self.createDataProducingThreads(1, self.getTestBatch)              

        def createInputGeneratorObject(self, threadID):
                '''Creates an instance of InputGenerator class for each thread using appropriate train indices. Test indices are same for all threads.

                Arguments required from json config file:
                1. datasetFiles
                2. trainIndices
                3. testIndices
                4. normalizationParams
                5. uniqueLabels
                6. keepPatientDataObjectsInMemory
                7. denoise - Dict of booleans. Curvature flow algorithm to be executed on what all modalities
                8. n4BiasFieldCorrection - Dict of booleans. N4 Bias Field Correction algorithm to be executed on what all modalities.
                '''

                inputGenerator = InputGenerator(self.config['datasetFiles'], self.config['trainIndices'][threadID], self.config['testIndices'], self.config['normalizationParams'], self.config['uniqueLabels'], self.config['keepPatientDataObjectsInMemory'], self.config['denoise'], self.config['n4BiasFieldCorrection'])
                return inputGenerator

        def createDataProducingThreads(self, numberOfThreads, targetFunction):
                '''Creates input training and test batches threads based on specified numberOfThreads and the target function.'''

                # Each thread has its own instance of InputGenerator
                # Instance of PatientDataObjectManager is same across all inputGenerator instances
                threads = {}
                for i in range(numberOfThreads):
                        inputGenerator = self.createInputGeneratorObject(str(i))
                        thread = Thread(target=targetFunction, args=(inputGenerator, ))
                        thread.start()
                        threadObj = {}
                        threadObj['thread'] = thread
                        threadObj['inputGenerator'] = inputGenerator
                        threads[i] = threadObj
                return threads

        def getTrainBatch(self, inputGenerator):
                '''Adds training batches to the trainBatchQueue obtained using the inputGenerator instance of the processing thread.'''
                
                # Thread finishes execution when exitSignal becomes True
                while True and self.exitSignal == False:
                        batches = inputGenerator.getNextTrainingBatches(self.config['patchSizes'],self.config['trainBatchPerLabel'],self.config['perInputShapes'], self.config['numBatchesPerPatient'], self.config['threeDPatches'], self.config['normalizeBatches'], self.config['balancedBatches'])
                        if batches is not None:
                                for batch in batches:
                                        if batch is not None and self.exitSignal == False:
                                                self.trainBatchQueue.put(batch)

        def getTestBatch(self, inputGenerator):
                '''Adds test batches to the testBatchQueue obtained using the inputGenerator instance of the processing thread.'''
                
                # Thread finishes execution when exitSignal becomes True
                while True and self.exitSignal == False:
                        batches = inputGenerator.getNextTestBatches(self.config['patchSizes'],self.config['testBatchPerLabel'],self.config['perInputShapes'], self.config['numBatchesPerPatient'], self.config['threeDPatches'], self.config['normalizeBatches'], self.config['balancedBatches'])
                        if batches is not None:
                                for batch in batches:
                                        if batch is not None and self.exitSignal == False:
                                                self.testBatchQueue.put(batch)

        def getBatchFromTrainQueue(self):
                '''Obtain training batch from the training queue in an asynchronous manner.'''

                # If queue empty exception is thrown, wait for certain time (queueGetRetryInterval from config file) and retry
                while True:
                        try:
                                batch = self.trainBatchQueue.get(False)
                                print "training batch obtained"
                                return batch
                        except Queue.Empty:
                                print ("training queue is empty, will retry in " + str(self.config['queueGetRetryInterval']) + " seconds")
                                time.sleep(self.config['queueGetRetryInterval'])

        def getBatchFromTestQueue(self):
                '''Obtain test batch from the test queue in an asynchronous manner.'''

                # If queue empty exception is thrown, wait for certain time (queueGetRetryInterval from config file) and retry again
                while True:
                        try:
                                batch = self.testBatchQueue.get(False)
                                print "test batch obtained"
                                return batch
                        except Queue.Empty:
                                print ("test queue is empty, will retry in " + str(self.config['queueGetRetryInterval']) + " seconds")
                                time.sleep(self.config['queueGetRetryInterval'])

        def getTestAccuracyAndF_Scores(self):
                '''Returns test accuracy and F-score of each class.'''

                # Fetch test(validation) batch from test queue
                # calculate accuracy
                # Get and display F-Score results using projectUtil instance taking prediction and true labels numpy array as its arguments
                xs_test, y_test = self.getBatchFromTestQueue()
                self.trainingModel.printTestAccuracy(xs_test, y_test)
                predictions = self.trainingModel.getPredictions(xs_test)
                y_true = self.trainingModel.getIntegerLabelListFromOneHotTensor(y_test)
                y_pred = self.trainingModel.getIntegerLabelListFromOneHotTensor(predictions)
                results = self.projectUtil.getFScoresResults(y_true,y_pred)
                self.projectUtil.printResults(results)
                #self.projectUtil.printResultsInLatexTabularFormat(results)

        def executeTrainingIteration(self, iterationNumber):
                '''Execute one training iteration.'''

                # Fetch a batch from the training queue
                # call executeTrainStep of the trainingModel instance
                # Print results on test(validation) data after certain number of iterations (testAccuracyCalculationIter from config file)
                start = time.time()
                xs_train,y_train = self.getBatchFromTrainQueue()
                self.trainingModel.executeTrainStep(xs_train, y_train, 0.5, iterationNumber)
                if (iterationNumber + 1) % self.config['testAccuracyCalculationIter'] == 0:
                        self.getTestAccuracyAndF_Scores()
                end = time.time()
                timeTaken = float(end - start) / 60.0
                print("time required for iteration: " + str(iterationNumber) + " = " + str(timeTaken) + " mins")	

        def saveParameters(self, iterationNumber):
                '''Save parameters of the training model to a file.'''

                modelFileName = self.trainingModel.getModelFileName(iterationNumber)
                self.trainingModel.saveParameters(self.trainingModel.getSaverObject(), modelFileName)

        def executeTrainingAlgorithm(self, numberOfIterations):
                '''Execute training algorithm based on the specified number of iterations and save parameters after training gets completed.'''
                
                start = time.time()
                for i in range(numberOfIterations):
                        self.executeTrainingIteration(i)
                        endUptilNow = time.time()
                        timeTakenUptilNow = float(endUptilNow - start) / 60.0
                        print("Total training time uptil now: " + str(timeTakenUptilNow) + " mins")
                end = time.time()
                timeTaken = float(end - start) / 60.0
                print("Total training time : " + str(timeTaken) + " mins")
                self.saveParameters(i + 1)
                self.finalizeExecution()

        def finalizeExecution(self):
                '''Called after completion of training and saving of model parameters.

                1. Send exit signal to all batch producing threads
                2. Empty the queue for threads in wait state to complete execution
                3. Wait for all the threads to join
                4. Main thread exits.
                '''

                self.exitSignal = True
                while self.trainBatchQueue.qsize() != 0:
                        self.trainBatchQueue.get()
                while self.testBatchQueue.qsize() != 0:
                        self.testBatchQueue.get()
                self.trainBatchQueue = None
                self.testBatchQueue = None
                print "Exiting..."
                time.sleep(1)
                for threadID in self.trainBatchProducingThreads:
                        threadObj = self.trainBatchProducingThreads[threadID]
                        threadObj['thread'].join()
                for threadID in self.testBatchProducingThreads:
                        threadObj = self.testBatchProducingThreads[threadID]
                        threadObj['thread'].join()
                sys.exit()
