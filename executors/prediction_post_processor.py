import sys, io
import os
import json
import numpy as np
from generic.input_generator import InputGenerator
from generic.project_util import ProjectUtil
from generic.mha_image import MhaImage
import time

class PredictionPostProcessor:

	#create onetime objects
	def __init__(self, configFile):
		self.exitSignal = False
		self.createObjects(configFile)

	#create onetime objects
	#1. Load config file
	#2. Instance of ProjectUtil
	def createObjects(self, configFile):
		with open(configFile) as jsonConfigFile:
			self.config = json.load(jsonConfigFile)
		self.projectUtil = ProjectUtil()
		self.inputGenerator = InputGenerator(self.config['datasetFiles'], None, None, self.config['normalizationParams'], self.config['uniqueLabels'])

	def postProcessImage(self, patientID, trueLabelImage, inputPredictionFilePath, denoisedOutputFilePath):
		print "post processing the image"
		predLabelImage = MhaImage(None, None, inputPredictionFilePath)
		predLabelImage.getArrayFromFile()
		postProcessingTechnique = getattr(self.projectUtil, self.config['postProcessPrediction'])
		denoisedImageArray = postProcessingTechnique(predLabelImage.completeImageArray)
		self.projectUtil.generateMhaImageFromImageArray(denoisedImageArray, denoisedOutputFilePath)
		print "\n\nWithout post processing the image"
		self.calculatePredictionResults(patientID, trueLabelImage, inputPredictionFilePath)
		print "\n\nWith post processing the image"
		self.calculatePredictionResults(patientID, trueLabelImage, denoisedOutputFilePath)

	#Using predicted image file path and true image file path, MhaImage objects are generated and F-Scores are calculated and displayed
	def calculatePredictionResults(self, patientID, trueLabelImage, outputFilePath):
		if trueLabelImage is not None:
			predLabelImage = MhaImage(None, None, outputFilePath)
			trueLabelImage.getArrayFromFile()
			predLabelImage.getArrayFromFile()
			self.projectUtil.comparePredictedImageWithTrueImage(trueLabelImage.completeImageArray,predLabelImage.completeImageArray, uniqueLabels=self.config['uniqueLabels'], predictionPatientID=patientID)
			self.projectUtil.comparePredictedImageWithTrueImageAccuracy(trueLabelImage.completeImageArray,predLabelImage.completeImageArray)
		return

	#Repeats the post processing step for all the indexes specified in the predictionIndices dict in the config file
	def postProcessPredictions(self):
		predictionFilesDir = str(self.config['trainingModelParams']['modelDir']) + '/' + str(self.config['predictionFilesDir'])
		if not os.path.exists(predictionFilesDir):
				os.makedirs(predictionFilesDir)
		for datasetFile in self.config['predictionIndices']:
			for patientIndex in self.config['predictionIndices'][datasetFile]:
				trueLabelImage = InputGenerator.patientDataObjectManager.getLabelImageObject(datasetFile, patientIndex)
				patientID = datasetFile + '_' + str(patientIndex)
				inputPredictionFilePath = str(predictionFilesDir) + '/' + str(patientID) + '.mha'
				denoisedOutputFilePath = str(predictionFilesDir) + '/' + str(patientID) + '_denoised.mha'
				self.postProcessImage(patientID, trueLabelImage, inputPredictionFilePath, denoisedOutputFilePath)
