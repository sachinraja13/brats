import sys, io
import numpy as np
from generic.mha_image import MhaImage
from generic.patient_data import PatientData
from generic.input_generator import InputGenerator
from executors.glioma_segmentor import GliomaSegmentor
from executors.glioma_segmentor_trainer import GliomaSegmentorTrainer
from executors.prediction_post_processor import PredictionPostProcessor
from generic.project_util import ProjectUtil
from crf_denoising.crf_image_denoiser_3d import CRFImageDenoiser3D
from crf_denoising.crf_image_denoiser_2d import CRFImageDenoiser2D
from executors.output_visualizer import OutputVisualizer

if __name__ == '__main__':
	configFile = sys.argv[1]
	gliomaSegmentorTrainer = GliomaSegmentorTrainer(configFile)
	gliomaSegmentorTrainer.executeTrainingAlgorithm(20)
	#gliomaSegmentor = GliomaSegmentor(configFile)
	#gliomaSegmentor.predictGliomaRegions()
	#predictionPostProcessor = PredictionPostProcessor(configFile)
	#predictionPostProcessor.postProcessPredictions()
	#outputVisualizer = OutputVisualizer(configFile)
	#outputVisualizer.prepareVisualizations(postProcessedPredictions=False)



	#i = MhaImage('tempDataset/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_T1.54513/VSD.Brain.XX.O.MR_T1.54513.mha')
	#i.writeMhaFileFromArray('out.mha', i.completeImageArray)
	#modalitiesPaths = {}
	#modalitiesPaths['T1'] = 'tempDataset/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_T1.54513/VSD.Brain.XX.O.MR_T1.54513.mha'
	#modalitiesPaths['T2'] = 'tempDataset/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_T2.54515/VSD.Brain.XX.O.MR_T2.54515.mha'
	#modalitiesPaths['T1C'] = 'tempDataset/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_T1c.54514/VSD.Brain.XX.O.MR_T1c.54514.mha'
	#modalitiesPaths['FLAIR'] = 'tempDataset/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512/VSD.Brain.XX.O.MR_Flair.54512.mha'
	#labelPath = 'tempDataset/brats_2013_pat0001_1/VSD.Brain_3more.XX.O.OT.54517/VSD.Brain_3more.XX.O.OT.54517.mha'
	#print "generating indexes"
	#d = PatientData(1, modalitiesPaths, labelPath)
	#print "generating batch"
	#x,y = d.getBalancedBatch(2,3,[2*2*4])
	#print x
	#print y
	#patchArray = d.getCompletePatch(32, 101, 77, 50)
	#c = MhaImage(None)
	#c.writeMhaFileFromArray('t.mha', patchArray)

	#datasetFiles = sys.argv[1:]
	#ig = InputGenerator(datasetFiles, None, None, None)
	#i1 = MhaImage(None, None, 'dataset/HGG/brats_2013_pat0001_1/VSD.Brain_3more.XX.O.OT.54517/VSD.Brain_3more.XX.O.OT.54517.mha')
	#i1.getArrayFromFile()
	#i2.getArrayFromFile()
	#projectUtil = ProjectUtil()
	#projectUtil.comparePredictedImageWithTrueImage(i1.completeImageArray,i2.completeImageArray,predictionPatientID="lgg_data_0")
	#i = MhaImage(None, None, 'lenet_caffe/predictions/hgg_data_0.mha')
	#i.getArrayFromFile(denoise=False)
	#crfImageDenoiser2D = CRFImageDenoiser2D(i.completeImageArray)
	#denoisedArray = crfImageDenoiser2D.denoise3DImageSliceBySlice()
	#noisyImageArray = i.completeImageArray
	#print noisyImageArray.dtype
	#denoisedArray = np.zeros(noisyImageArray.shape)
	#for z in range(noisyImageArray.shape[0]):
	#	noisyArray = noisyImageArray[z,:,:]
	#	crfImageDenoiser2D = CRFImageDenoiser2D(noisyArray)
	#	print z
	#	if not np.any(noisyArray):
	#		continue
	#	denoisedArray[z,:,:] = crfImageDenoiser2D.executeCRF()
	#	del(crfImageDenoiser2D)
	#denoisedArray = projectUtil.postProcessPredictionUsingCRFDenoising2D(noisyImageArray)
	#projectUtil.generateMhaImageFromImageArray(denoisedArray, 'denoised.mha')
	#projectUtil.comparePredictedImageWithTrueImage(i1.completeImageArray,i.completeImageArray, uniqueLabels=5, predictionPatientID='hgg_data_0')
	#projectUtil.comparePredictedImageWithTrueImageAccuracy(i1.completeImageArray,i.completeImageArray)
	#projectUtil.comparePredictedImageWithTrueImage(i1.completeImageArray,denoisedArray, uniqueLabels=5, predictionPatientID='hgg_data_0')
	#projectUtil.comparePredictedImageWithTrueImageAccuracy(i1.completeImageArray,denoisedArray)

