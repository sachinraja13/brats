import tensorflow as tf
import sys, io
import os

NUM_CORES = 8

class BaseTensorflowModel(object):

        #Initializes the base tensorflow model
        #trainingModelParams - dictionary:
        #modelName: used for obtaing the name of the caffe model file to be saved (modelName_<number of iterations>.caffemodel)
        #modelDir: Directory of the model where all the prototxt files, extending python module, config file, predictions and saved parameters exist relative to the brats main directory.
        #input shapes: list of [None, each element of input shape] for every input shape in input shapes. None signifies variable batch size.
        #output shape: [None, number of unique labels]. None signifies variable batch size.
        def __init__(self, trainingModelParams):
                self.sess = tf.Session(config=tf.ConfigProto(inter_op_parallelism_threads=NUM_CORES,intra_op_parallelism_threads=NUM_CORES))
                self.modelName = trainingModelParams['modelName']
                self.modelDir = trainingModelParams['modelDir']
                self.learntModelsDir = trainingModelParams['learntModelsDir']
                self.inputShapes = trainingModelParams['inputShapes']
                self.outputShape = trainingModelParams['outputShape']
                if self.modelDir is not None and not os.path.exists(self.modelDir):
                        os.makedirs(self.modelDir)
                if self.learntModelsDir is not None and not os.path.exists(self.modelDir + '/' + self.learntModelsDir):
                        os.makedirs(self.modelDir + '/' + self.learntModelsDir)
                self.xs = []
                #Create a place holder for every input batch of patches in case of cascaded architectures.
                for i in range(len(self.inputShapes)):
                        x = tf.placeholder("float", shape=self.inputShapes[i])
                        self.xs.append(x)
                #Create a single place holder for labels.
                self.y_ = tf.placeholder("float", shape=self.outputShape)
                self.numberOfLabels = self.outputShape[1]
                #Create a single place holder for feeding dropout probability of the network
                self.keep_prob = tf.placeholder("float")

        #Create a node of type variable with specified shape and initialize using truncated normal distribution with a standard deviation of 0.1
        def weight_variable(self, shape):
                initial = tf.truncated_normal(shape, stddev=0.1)
                return tf.Variable(initial)

        #Create a node of bias of type variable which is a single value initialized with 0.1
        def bias_variable(self, shape):
                initial = tf.constant(0.1, shape=shape)
                return tf.Variable(initial)

        #Create a 2-D convolutional layer with specified input, weight variable, strides and padding according to the tensorflow API.
        def conv2d(self, x, W, strides=[1, 1, 1, 1], padding='SAME'):
                return tf.nn.conv2d(x, W, strides=strides, padding=padding)

        #Create a max pool layer with specified input, kernel size=[1,2,2,1], strides=[1,2,2,1] and padding according to the tensorflow API.
        def max_pool_2x2(self, x):
                return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        #Create a max pool layer with specified input, kernel size, strides and padding according to the tensorflow API.
        def max_pool(self, x, kernelSize, strides, padding='SAME'):
                return tf.nn.max_pool(x, ksize=kernelSize, strides=strides, padding=padding)

        #Prepare feed dictionary using the generated batch for executing the graph using session object of tensorflow API
        def prepareFeedDict(self, train_xs, train_y, keep_prob):
                feedDict={}
                if train_y is not None:
                        feedDict[self.y_] = train_y
                feedDict[self.keep_prob] = keep_prob
                for i in range(len(train_xs)):
                        x = train_xs[i]
                        if x is not None:
                                feedDict[self.xs[i]] = x
                return feedDict

        #Calucate training accuracy and execute one step of training
        def executeTrainStep(self, train_xs, train_y, keep_prob_train_step, stepNumber):
                feedDict = self.prepareFeedDict(train_xs, train_y, 1.0)
                train_accuracy = self.accuracy.eval(session=self.sess, feed_dict=feedDict)
                print("\nstep %d, training accuracy %g"%(stepNumber, train_accuracy))
                feedDict = self.prepareFeedDict(train_xs, train_y, keep_prob_train_step)
                self.train_step.run(session=self.sess, feed_dict=feedDict)

        #Calculate and display accuracy on test (validation) batch
        def printTestAccuracy(self, test_xs, test_y):
                feedDict = self.prepareFeedDict(test_xs, test_y, 1.0)
                print("\ntest accuracy %g"%self.accuracy.eval(session=self.sess, feed_dict=feedDict))

        #Gets the model file name using which parameters of the tensorflow model are saved in base folder relative to the working model directory
        def getModelFileName(self, iteration):
                modelFileName = self.modelName + '_' + str(iteration) + '.ckpt'
                return modelFileName

        #Save parameters in the specified model file using tensorflow saver object and session object.
        def saveParameters(self, saverObj, modelFileName):
                modelFilePath = self.modelDir + '/' + self.learntModelsDir + '/' + modelFileName
                savedPath = saverObj.save(self.sess, modelFilePath)
                print("Model saved : " + str(savedPath))

        #Load parameters into the model architecture from specified file using tensorflow saver object and session object.
        def loadParameters(self, saverObj, modelFileName):
                if modelFileName is None:
                        return None
                modelFilePath = self.modelDir + '/' + self.learntModelsDir + '/' + modelFileName
                saverObj.restore(self.sess, modelFilePath)
                print("Model restored : " + str(modelFilePath))

        #Get integer list of labels from the probabilities tensor returned by tensorflow. Output is a list of size [Batch Size]
        def getIntegerLabelListFromOneHotTensor(self, y):
                return tf.argmax(y, 1).eval(session=self.sess).tolist()
