#Config File Explanation


1. **normalizationParams** : Type: Dictionary. Required for normalizing the image. Either linear or non-linear normalization is performed depending on what method is called for normalizing the image. This is done after subtracting mean from the image and dividing by standard deviation for each modality.

2. **datasetFiles** : Type: List. Specifies location of the individual dataset files relative to the base brats folder.

3. **denoise** : Type: Dictionary. Keys are modalities and values are boolean specifying whether to execute SimpleITK's Curvature Flow algorithm on the MHA image as a preprocessing step.

4. **n4BiasFieldCorrection** : Type: Dictionary. Keys are modalities and values are boolean specifying whether to execute SimpleITK's N4 Bias Field Correction algorithm on the MHA image as a preprocessing step. Ideally should be performed only on T1 and T1C modalities.

5. **trainIndices** : Type: Dictionary. Multiple threads can generate input batches and store those in a synchronised queue. Key of this dictionary is the threadID and value is again a dictionary with key being the dataset file name and value being a list of indexes of patients to be used for generating input batches for training.

6. **testIndices** : Type: Dictionary. Key of this dictionary is the dataset file name and value being a list of indexes of patients to be used for generating input batches for test/validation.

7. **predictionIndices** : Type: Dictionary. Key of this dictionary is the dataset file name and value being a list of indexes of patients to be used for generating prediction MHA images.

8. **trainingModelParams** : Type: Dictionary.

    * *cascadedArchitecture (Only for caffe models)* : Type: Boolean. Specifies whether to use cascaded architectures or not. In case of cascaded architectures, outputs from pretrained models (output blob = cascadedoutput) is appended to the input for the current model. The last element of the patchSizes is used to generate input for current model and previous ones are used as inputs for cascaded architectures.
  
    * *cascadedNetworksFolder (Only for caffe models)* : Type: String. Name of the folder that contains deploy prototxt files and parameters files to generate cascaded outputs which are depthwise concatenated to the input for the current model. This folder resides inside the model directory.
  
    * *cascadedDeployNetPrototxts (Only for caffe models)* : Type: List. List of deploy prototxt files of cascade models.
    
    * *cascadedNetsModelFiles (Only for caffe models)*: Type: List. List of corresponding parameters files for cascade models deploy prototxt files.
    
    * *trainOrPredict* : Type: String. Takes values 'train' or 'predict'. Configures the code for training or making predictions.

    * *solverPrototxtFile (Only for caffe models)* : Type: String. Name of the solver prototxt file that resides in the model folder.

    * *predictPrototxtFile (Only for caffe models)* : Type: String. Name of the deploy prototxt file that resides in the model folder and used for making predictions. Please note that this prototxt does not contain the loss layer.

    * *trainTestPrototxtFile (Only for caffe models)* : Type: String. Name of the train/validation prototxt file that resides in the model folder and used for training the model. Please note that this prototxt contains the loss layer.

    * *modelName* : Type: String. Name of the model. Used for generating the names of saved parameters file.

    * *learntModelsDir* : Type: String. Name of the directory relative to the current model directory used for saving the parameters files.

    * *modelDir* : Type: String. Path of the model directory relative to the brats directory (where segmentor.py resides).

9. **keepPatientDataObjectsInMemory** : Type: Boolean. Specifies whether to keep all patient data objects in memory or not. If false, patient data is read again from files.

10. **predictionFilesDir** : Type: String. Directory where prediction MHA files are stored. This path is relative to the model directory. '_denoised' is appended to the prediction files that have been post-processed.

11. **visualizationsDir** : Type: String. Directory relative to the current model directory where visualizations are stored.

12. **testAccuracyCalculationIter** : Type: Integer. Specifies number of training iterations after which results on validation/test data is calculated.

13. **queueGetRetryInterval** : Type: Integer. Wait for this much time in seconds before trying to get data from queue again. The get operation from the queue is asynchronized.

14. **uniqueLabels** : Type: Integer. Specifies number of unique labels in which each pixel is categorized into during the learning/prediction process.

15. **moduleName** : Type: String. Name of the python file that extends a base model and has model specific code.

16. **maxQueueSize** : Type: Integer. Maxinum size of the queue that holds train or validation batches for processing.

17. **numBatchesPerPatient** : Type: Integer. Specifies number of training and test batches generated per patient.

18. **threeDPatches** : Type: Boolean. Specifies whether to generate input patches along just xy plane or xy, xz and yz planes per coordinate.

19. **normalizeBatches** : Type: Boolean. Specify true if input patches are to be generated from normalized image, false otherwise.

20. **parametersFile** : Type: String or null. Name of the saved parameters file inside the learntModels directory. For prediction or resuming of training, specify filename, null otherwise.

21. **perInputShapes** : Type: List. List specifying dimensions of each input in the batch. Example, [10,10,4] would mean batch dimensions: [batchSize,10,10,4]. This should agree: batchSize * each element of perInputShape should be equal to batchSize*patchSize*patchSize*numberOfModalities.

22. **patchSizes** : Type: List. List of side lengths in pixels of square patches around a coordinate.

23. **balancedBatches** : Type: Boolean. Specify true to generate balanced or almost balanced batches (when data for any label is absent), false to generate patches around randomly selected non-zero coordinates.

24. **trainBatchPerLabel** : Type: Integer. Specifies number of input samples per label in the training batch.

25. **testBatchPerLabel** : Type: Integer. Specifies number of input samples per label in the test/validation batch.

26. **predictionBatchSize** : Type: Integer. Specifies number of input samples in the prediction generating batch.

27. **numPredBatchesGenThreads** : Type: Integer. Specifies number of prediction batches generating threads.

28. **predCoordinatesQueueMaxSize** : Type: Integer. Specifies maximum size of the queue that holds coordinates for which input patches are to be generated for prediction.

29. **postProcessPrediction** : Type: String. Specify name of the method from project_util.py file which is to be used for post-processing the prediction image.

30. **predBatchesQueueMaxsize** : Type: Integer. Specifies maximum size of the queue that holds input batches for prediction.