import sys, io
from caffe_base.base_caffe_model import BaseCaffeModel


class TrainingModel(BaseCaffeModel):

  #inputShape = [None, 784*4], outputShape = [None, 5]
  	def __init__(self, trainingModelParams):
		super(TrainingModel, self).__init__(trainingModelParams)