import sys, io
import tensorflow as tf
from tensorflow_base.base_tensorflow_model import BaseTensorflowModel


class TrainingModel(BaseTensorflowModel):

  #inputShape = [None, 784*4], outputShape = [None, 5]
  	def __init__(self, trainingModelParams):
		super(TrainingModel, self).__init__(trainingModelParams)
		self.generateComputationGraph()

	def generateComputationGraph(self):
		W_conv1 = self.weight_variable([5, 5, 4, 32])
		b_conv1 = self.bias_variable([32])
		x_image = tf.reshape(self.xs[0], [-1,28,28,4])
		h_conv1 = tf.nn.relu(self.conv2d(x_image, W_conv1) + b_conv1)
		h_pool1 = self.max_pool_2x2(h_conv1)

		W_conv2 = self.weight_variable([5, 5, 32, 64])
		b_conv2 = self.bias_variable([64])

		h_conv2 = tf.nn.relu(self.conv2d(h_pool1, W_conv2) + b_conv2)
		h_pool2 = self.max_pool_2x2(h_conv2)

		W_fc1 = self.weight_variable([7 * 7 * 64, 1024])
		b_fc1 = self.bias_variable([1024])

		h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
		h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

		h_fc1_drop = tf.nn.dropout(h_fc1, self.keep_prob)

		W_fc2 = self.weight_variable([1024, 5])
		b_fc2 = self.bias_variable([5])

		y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
		self.predictions = y_conv
		self.cross_entropy = -tf.reduce_sum(self.y_*tf.log(y_conv))
		self.train_step = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy)
		self.correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(self.y_,1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_prediction, "float"))
		self.sess.run(tf.initialize_all_variables())

		self.saver = tf.train.Saver()

	def getPredictions(self, xs):
		feedDict = self.prepareFeedDict(xs, None, 1.0)
		return self.predictions.eval(session=self.sess, feed_dict=feedDict)

	def getSaverObject(self):
		return self.saver
