import sys, io
from generic.mha_image import MhaImage
import numpy as np
import random
import cPickle
import os
from threading import Thread
import Queue
import time

class PatientData:

        #modalitiesPaths is a dict with keys as T1, T1C, T2 and FLAIR and values are paths 
        #patientID: string - datasetFile + patientIndex
        #modalitiesPaths: Dict - key:modalityName, value:filePath
        #labelImageFilePath: labelImageFilePath
        #normalizationParams: normalization parameters dict
        #uniqueLabels: Number of unique labels
        def __init__(self, patientID, modalitiesPaths, labelImageFilePath, normalizationParams, uniqueLabels, denoise, n4BiasFieldCorrection):
                self.patientID = patientID
                self.labelImageFilePath = labelImageFilePath
                self.uniqueLabels = uniqueLabels
                self.patientScanData = {}
                for modality in modalitiesPaths:
                        self.patientScanData[modality] = MhaImage(patientID, modality, modalitiesPaths[modality])
                        self.patientScanData[modality].getArrayFromFile(denoise, n4BiasFieldCorrection)
                        if normalizationParams is not None:
                                self.patientScanData[modality].getNormalizedImageArray(normalizationParams)
                self.labelData = MhaImage(patientID, 'LABEL', labelImageFilePath)
                #Do not denoise the label image.
                self.labelData.getArrayFromFile()
                self.imageShape = self.labelData.completeImageArray.shape
                self.numberOfModalities = len(self.patientScanData)
                self.getCoordinatesForEachLabel()
                #X, Y, Z coordinates for sequential access
                self.predX = 0
                self.predY = 0
                self.predZ = 0

                self.allNonZeroIndexes = None


        #Get the next coordinate in sequential access
        def getNextCoordinate(self):
                if self.predY is not None and self.predX is not None and self.predZ is not None:
                        self.predY = self.predY + 1
                        if self.predY >= self.imageShape[2]:
                                self.predX = self.predX + 1
                                self.predY = 0
                                if self.predX >= self.imageShape[1]:
                                        self.predZ = self.predZ + 1
                                        self.predX = 0
                        if self.predZ >= self.imageShape[0]:
                                self.predX = None
                                self.predY = None
                                self.predZ = None
                                return None
                return (self.predZ, self.predX, self.predY)


        #Returns true if all modalities at z, x, y are 0. Else, returns False
        def areAllModalitiesPixelsZero(self, z, x, y):
                self.allModalitiesPixelsZeroflag = True
                for modality in self.patientScanData:
                        if self.patientScanData[modality].completeImageArray[z, x, y] != 0:
                                self.allModalitiesPixelsZeroflag = False
                if self.allModalitiesPixelsZeroflag == True:
                        return True
                else:
                        return False


        #Gets a dict. Key: label, Value: non zero coordinates that belong to that label. Reads from pickle file. If file not present, creates one.
        #Data structures: 
        #1. labelCoordinates: dict, key: label, value: cordinates of that label
        #2. numberOfCategories: total number of labels in a patient
        #3. numberOfDataPointsByLabel: dict, key: label, value: number of coordinates of that label
        #4. patientLabelList: List of labels of a patient
        def getCoordinatesForEachLabel(self):
                dirName = 'labelCoordinateDicts'
                if not os.path.exists(dirName):
                        os.makedirs(dirName)
                fileName = str(dirName) + '/labelCoordinatesDict_' + str(self.patientID) + '.pickle'
                try:
                        with open(fileName, 'rb') as handle:
                                self.labelCoordinates = cPickle.load(handle)
                except:
                        print "label dict file not present, creating one"
                        self.getCoordinatesForEachLabelAndSaveToFile(fileName)
                self.numberOfCategories = len(self.labelCoordinates)
                self.numberOfDataPointsByLabel = {}
                for label in self.labelCoordinates:
                        self.numberOfDataPointsByLabel[label] = len(self.labelCoordinates[label])
                self.patientLabelList = list(self.numberOfDataPointsByLabel.keys())

        #gets coordinates for each label and saves the dictionary to the pickle file. Calls if read from pickle file throws an exception. File read based is based on patientID.
        def getCoordinatesForEachLabelAndSaveToFile(self, fileName):
                self.labelCoordinates = {}
                for z in xrange(self.imageShape[0]):
                        for x in xrange(self.imageShape[1]):
                                for y in xrange(self.imageShape[2]):
                                        category = self.labelData.completeImageArray[z, x, y]
                                        if category == 0 and self.areAllModalitiesPixelsZero(z, x, y) == True:
                                                continue
                                        if category not in self.labelCoordinates:
                                                self.labelCoordinates[category] = []
                                        self.labelCoordinates[category].append((z, x, y))
                try:
                        with open(fileName, 'wb') as handle:
                                cPickle.dump(self.labelCoordinates, handle)
                except:
                        print "error saving label coordinate dictionary to file"


        #Gets a patch of size patchSize*patchSize in xy plane for input 3d coordinate across all modalities. Generates normalized patch if normalized=True
        def getXYPatch(self, z, x, y, patchSize, normalized=True):
                patchAllModalities = np.zeros((patchSize, patchSize, self.numberOfModalities), dtype=np.float32)
                i = 0
                for modality in self.patientScanData:
                        patchAllModalities[:, :, i] = self.patientScanData[modality].getXYPatchAroundCoordinate(z, x, y, patchSize, normalized)
                        i = i + 1
                return patchAllModalities


        #Gets three patches of size patchSize*patchSize in xy, xz and yz planes for input 3d coordinate across all modalities. Generates normalized patch if normalized=True
        def getThreeDimensionalPatch(self, z, x, y, patchSize, normalized=True):
                threeDimPatchAllModalities = np.zeros((3, patchSize, patchSize, self.numberOfModalities), dtype=np.float32)
                i = 0
                for modality in self.patientScanData:
                        xyPatch, zxPatch, zyPatch = self.patientScanData[modality].getThreeDimensionalPatch(z, x, y, patchSize, normalized)
                        threeDimPatchAllModalities[0, :, : ,i] = xyPatch
                        threeDimPatchAllModalities[1, :, : ,i] = zxPatch
                        threeDimPatchAllModalities[2, :, : ,i] = zyPatch
                        i = i + 1
                return threeDimPatchAllModalities


        #Gets inputs for the training or test(validation) batches.
        #Arguments:
        #1. patchSize: side length in pixels of a square patch around a coordinate.
        #2. perInputShape: list specifying dimensions of each input in the batch. Example, [10,10,4] would mean batch dimensions: [batchSize,10,10,4]. This should agree: batchSize * each element of perInputShape should be equal to batchSize*patchSize*patchSize*numberOfModalities.
        #3. labelCoordinateIndexes: Dict- key: label, value: coordinates of the label from which imput patches are to be generated
        #4. totalBatchSize: total size of the input batch
        #5. threeDPatches: boolean. If true, patches along three planes are generated for each coordinate and totalBatchSize = totalBatchSize * 3.
        #6. normalized: boolean. If true, patches are obtained from the normalized image array.
        #Output:
        #1. Returns a numpy array: [totalBatchSize, each element of perInputShape]
        def getXsForBatch(self, patchSize, perInputShape, labelCoordinateIndexes, totalBatchSize, threeDPatches, normalized=True):
                if threeDPatches:
                        inputShape = [totalBatchSize * 3]
                        x = np.zeros((totalBatchSize * 3, patchSize, patchSize, self.numberOfModalities))
                else:
                        inputShape = [totalBatchSize]
                        x = np.zeros((totalBatchSize, patchSize, patchSize, self.numberOfModalities))

                inputShape.extend(list(perInputShape))

                i = 0
                j = 0
                for label in labelCoordinateIndexes:
                        coordinateIndexes = labelCoordinateIndexes[label]
                        for index in coordinateIndexes:
                                coordinate = self.labelCoordinates[label][index]
                                if threeDPatches:
                                        patches = self.getThreeDimensionalPatch(coordinate[0], coordinate[1], coordinate[2], patchSize, normalized)
                                        x[j] = patches[0]
                                        x[j+1] = patches[1]
                                        x[j+2] = patches[2]
                                else:
                                        x[i] = self.getXYPatch(coordinate[0], coordinate[1], coordinate[2], patchSize, normalized)
                                i = i + 1
                                j = j + 3
                x = x.reshape(inputShape)
                return x


        #Gets true labels for the training or test(validation) batches
        #Arguments:
        #1. labelCoordinateIndexes: Dict- key: label, value: coordinates of the label from which imput patches are to be generated
        #2. totalBatchSize: total size of the input batch
        #3. threeDPatches: boolean. If true, patches along three planes are generated for each coordinate and totalBatchSize = totalBatchSize * 3.
        #Output:
        #1. Returns a numpy array: [totalBatchSize, number of unique labels] - Each true label is in the form of a one hot vector. 
        def getYsForBatch(self, labelCoordinateIndexes, totalBatchSize, threeDPatches):
                if threeDPatches:
                        loopIterations = 3
                        ys = np.zeros((totalBatchSize * 3, self.uniqueLabels), dtype=np.float32)
                else:
                        loopIterations = 1
                        ys = np.zeros((totalBatchSize, self.uniqueLabels), dtype=np.float32)
                i = 0
                for label in labelCoordinateIndexes:
                        coordinateIndexes = labelCoordinateIndexes[label]
                        for index in coordinateIndexes:
                                for loopIteration in range(loopIterations):
                                        y = np.zeros(self.uniqueLabels, dtype=np.float32)
                                        y[label] = 1.0
                                        ys[i] = y
                                        i = i + 1
                return ys

        #Randomize input patches and true labels along the axis of batch size. Outputs randomized batch.
        def randomizeBatch(self, xs, ys):
                zippedList = []
                zippedList.append(ys)
                for x in xs:
                        zippedList.append(x)
                tmp = list(zip(*zippedList))
                np.random.shuffle(tmp)
                randomizedList = zip(*tmp)
                ys = np.array(randomizedList[0])
                xs = []
                for i in range(1, len(randomizedList)):
                        xs.append(np.array(randomizedList[i]))
                return (xs, ys)


        #generates a balanced (almost balanced batch) for training and test(validation purposes)
        #Arguments:
        #1. patchSizes: list of patch sizes to generate multiple patches around a coordinate for cascaded architectures
        #2. batchSizePerLabel: number of input samples per label in the batch. All patients might not have all labels. In that case, batch is completed by adding extra coordinates randomly from all the available labels. totalBatchSize = batchSizePerLabel * number of unique labels.
        #3. perInputShapes: list of per input shapes to generate multiple patches around a coordinate for cascaded architectures
        #4. threeDPatches: boolean. If true, patches along three planes are generated for each coordinate and totalBatchSize = totalBatchSize * 3.
        #5. normalized: boolean. If true, patches are obtained from the normalized image array.
        #Output:Balanced batch, tuple of inputs(list of numpy arrays for each of the patch sizes and corresponding input shapes) and outputs(One hot vector of labels for each input in the batch)
        def getBalancedBatch(self, patchSizes, batchSizePerLabel, perInputShapes, threeDPatches=False, normalized=True):
                expectedTotalBatchSize = batchSizePerLabel * self.uniqueLabels
                if batchSizePerLabel > min(self.numberOfDataPointsByLabel.values()):
                        batchSizePerLabel = min(self.numberOfDataPointsByLabel.values())

                labelCoordinateIndexes = {}
                totalBatchSize = 0
                for label in self.labelCoordinates:
                        coordinateIndexes = random.sample(range(0, self.numberOfDataPointsByLabel[label]), batchSizePerLabel)
                        totalBatchSize = totalBatchSize + len(coordinateIndexes)
                        labelCoordinateIndexes[label] = coordinateIndexes

                while totalBatchSize < expectedTotalBatchSize:
                        label = random.choice(self.patientLabelList)
                        coordinateIndex = random.randint(0, self.numberOfDataPointsByLabel[label]-1)
                        labelCoordinateIndexes[label].append(coordinateIndex)
                        totalBatchSize = totalBatchSize + 1
                ys = self.getYsForBatch(labelCoordinateIndexes, totalBatchSize, threeDPatches)
                xs = []
                for i in range(len(patchSizes)):
                        patchSize = patchSizes[i]
                        perInputShape = perInputShapes[i]
                        x = self.getXsForBatch(patchSize, perInputShape, labelCoordinateIndexes, totalBatchSize, threeDPatches)
                        xs.append(x)

                batch = self.randomizeBatch(xs, ys)
                return batch

        #generates an unbalanced batch for training and test(validation purposes)
        #Arguments:
        #1. patchSizes: list of patch sizes to generate multiple patches around a coordinate for cascaded architectures
        #2. batchSizePerLabel: number of input samples per label in the batch. All patients might not have all labels. In that case, batch is completed by adding extra coordinates randomly from all the available labels. totalBatchSize = batchSizePerLabel * number of unique labels.
        #3. perInputShapes: list of per input shapes to generate multiple patches around a coordinate for cascaded architectures
        #4. threeDPatches: boolean. If true, patches along three planes are generated for each coordinate and totalBatchSize = totalBatchSize * 3.
        #5. normalized: boolean. If true, patches are obtained from the normalized image array.
        #Output:Unbalanced batch, tuple of inputs(list of numpy arrays for each of the patch sizes and corresponding input shapes) and outputs(One hot vector of labels for each input in the batch)
        def getUnbalancedBatch(self, patchSizes, batchSizePerLabel, perInputShapes, threeDPatches=False, normalized=True):
                if self.allNonZeroIndexes is None:
                        self.allNonZeroIndexes = []
                        for label in self.labelCoordinates:
                                for coordinateIndex in range(len(self.labelCoordinates[label])):
                                        self.allNonZeroIndexes.append((label, coordinateIndex))

                expectedTotalBatchSize = batchSizePerLabel * self.uniqueLabels
                
                labelCoordinateIndexes = {}
                totalBatchSize = 0
                while totalBatchSize < expectedTotalBatchSize:
                        labelCoordinateIndexTuple = random.choice(self.allNonZeroIndexes)
                        label = labelCoordinateIndexTuple[0]
                        coordinateIndex = labelCoordinateIndexTuple[1]
                        if label not in labelCoordinateIndexes:
                                labelCoordinateIndexes[label] = []
                        labelCoordinateIndexes[label].append(coordinateIndex)
                        totalBatchSize = totalBatchSize + 1                

                ys = self.getYsForBatch(labelCoordinateIndexes, totalBatchSize, threeDPatches)
                xs = []
                for i in range(len(patchSizes)):
                        patchSize = patchSizes[i]
                        perInputShape = perInputShapes[i]
                        x = self.getXsForBatch(patchSize, perInputShape, labelCoordinateIndexes, totalBatchSize, threeDPatches)
                        xs.append(x)

                batch = self.randomizeBatch(xs, ys)
                return batch
