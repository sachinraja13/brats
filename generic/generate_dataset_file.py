import sys, io
import os


def sortInOrder(patientFileList):
	sortedList = []
	flairFile = patientFileList[0]
	for i in range(1,4):
		sortedList.append(patientFileList[i])
	sortedList.append(flairFile)
	sortedList.append(patientFileList[4])
	return sortedList

def getPatientsFilesList(baseFolder):
	patients = []
	for directory in os.listdir(baseFolder):
		patientDir = baseFolder + '/' + directory
		if os.path.isdir(patientDir):
			patientMRIsDirs = os.listdir(patientDir)
			patientFileList = []
			for modelDir in patientMRIsDirs:
				patientModelDir = patientDir + '/' +modelDir
				if os.path.isdir(patientModelDir):
					for f in os.listdir(patientModelDir):
						if '.mha' in f:
							patientFileList.append(patientModelDir + '/' +f)
			patients.append(sortInOrder(patientFileList))
	return patients

def printInFormat(patients):
	for p in patients:
		s = ""
		for i in range(len(p)):
			if i < len(p) - 1:
				s = s + p[i] + " "
			else:
				s = s + p[i]
		print s


def main():
	baseFolder = sys.argv[1]
	patients = getPatientsFilesList(baseFolder)
	printInFormat(patients)


if __name__ == '__main__':
	main()