import sys, io

class ImageIndexManager:

	def __init__(self, imageShape):
		self.imageShape = imageShape
		self.maxIndex = 1
		for maxCoordinate in imageShape:
			self.maxIndex = self.maxIndex * maxCoordinate

	def coordinateToIndex(self, x, y, z):
		max_y, max_z = self.imageShape[1:]
		return z + y * max_y + x * max_z * max_y

	def indexToCoordinate(self, index):
		max_y, max_z = self.imageShape[1:]
		z = index % max_z
		index /= max_z
		y = index % max_y
		index /= max_y
		x = index
		return x, y, z
