import sys, io
from generic.patient_data import PatientData
from generic.mha_image import MhaImage

#Class that manages all the patient data objects for training and test(validation)
class PatientDataObjectManager:

        #Initialize PatientDataObjectManager. Keep single instance throughout for better performance in terms of memory
        #Arguments:
        #1. All file paths of patients by dataset file type.
        #2. Image normalization params
        #3. Number of unique labels for supervised classification and learning
        #4. keepPatientDataObjectsInMemory: If true, the patient data objects are kept in memory 
        def __init__(self, patientsFilePathsByType, normalizationParams, uniqueLabels, keepPatientDataObjectsInMemory=False, denoise=None, n4BiasFieldCorrection=None):
                self.keepPatientDataObjectsInMemory = keepPatientDataObjectsInMemory
                self.denoise = denoise
                self.n4BiasFieldCorrection = n4BiasFieldCorrection
                self.patientsFilePathsByType = patientsFilePathsByType
                self.normalizationParams = normalizationParams
                self.uniqueLabels = uniqueLabels
                self.inMemoryPatientDataObjects = {}

        #Generates patient data object using dataset file type and patient index number
        def getPatientDataObject(self, datasetFile, index):
                patientID = datasetFile + '_' + str(index)
                patientFilePaths = self.patientsFilePathsByType[datasetFile][patientID]
                patientData = self.getPatientObject(patientID, patientFilePaths)
                return patientData

        #Generates all the patient data objects. Does not store them in memory. Function can be used to just populate labelDict pickle files for all patients.
        def generatePatientDataObjects(self):
                for datasetFile in self.patientsFilePathsByType:
                        for patientID in self.patientsFilePathsByType[datasetFile]:
                                print("Processing patientID : " + str(patientID))
                                patientDataObject = PatientData(patientID, self.patientsFilePathsByType[datasetFile][patientID][0], self.patientsFilePathsByType[datasetFile][patientID][1], self.normalizationParams, self.uniqueLabels, self.denoise, self.n4BiasFieldCorrection)

        #Generates patient data object based on patientID and patientFilePaths as arguments 
        def getPatientObject(self, patientID, patientFilePaths):
                if patientID in self.inMemoryPatientDataObjects:
                        return self.inMemoryPatientDataObjects[patientID]
                patientData = PatientData(patientID, patientFilePaths[0], patientFilePaths[1], self.normalizationParams, self.uniqueLabels, self.denoise, self.n4BiasFieldCorrection)
                if self.keepPatientDataObjectsInMemory:
                        self.inMemoryPatientDataObjects[patientID] = patientData
                return patientData

        #Gets Label Image Object
        def getLabelImageObject(self, datasetFile, index):
                patientID = datasetFile + '_' + str(index)
                labelFilePath = self.patientsFilePathsByType[datasetFile][patientID][1]
                labelMhaImage = MhaImage(None, None, labelFilePath)
                labelMhaImage.getArrayFromFile()
                return labelMhaImage
