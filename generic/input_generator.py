import sys, io
from generic.patient_data import PatientData
from generic.patient_data_object_manager import PatientDataObjectManager

class InputGenerator:

        #Static PatientDataObjectManager object of the InputGenerator class.
        patientDataObjectManager = None

        #Initialize InputGenerator object. Used for generating input batches for training and testing from specified patient datafiles and indices.
        #Arguments:
        #1. datasetFiles: list - each file contains file paths of all patients in a purticular type of glioma case.
        #2. trainingPatientIDs: dict - key: datasetFile, value: list of patient indexes to be used for training.
        #3. testPatientIDs: dict - key: datasetFile, value: list of patient indexes to be used for testing(validation).
        #4. Image normalization parameters
        #5. Number of unique labels
        #6. boolean variable whether to keep patient data objects in memory for all the patient objects accessed.
        def __init__(self, datasetFiles, trainingPatientIDs, testPatientIDs, normalizationParams, uniqueLabels, keepPatientDataObjectsInMemory=False, denoise=None, n4BiasFieldCorrection=None):
                self.trainingPatientIDs = trainingPatientIDs
                self.testPatientIDs = testPatientIDs
                self.currentTrainPatientIndices = {}
                self.currentTestPatientIndices = {}
                self.patientsFilePathsByType = self.readDatasetFiles(datasetFiles)
                if InputGenerator.patientDataObjectManager is None:
                        InputGenerator.patientDataObjectManager = PatientDataObjectManager(self.patientsFilePathsByType, normalizationParams, uniqueLabels, keepPatientDataObjectsInMemory, denoise, n4BiasFieldCorrection)
                self.datasetFileToIndexMapping = self.patientsFilePathsByType.keys()
                self.currentTrainTypeIndex = 0
                self.currentTestTypeIndex = 0

        #Reads the dataset files and creates dictionary data structure for all the patientsFilePathsByType.
        def readDatasetFiles(self, datasetFiles):
                patientsFilePathsByType = {}
                for datasetFile in datasetFiles:
                        patientsFilePaths = {}
                        fileParts = datasetFile.split('/')
                        fileName = fileParts[len(fileParts) - 1]
                        self.currentTrainPatientIndices[fileName] = 0
                        self.currentTestPatientIndices[fileName] = 0
                        i = 0
                        with open(datasetFile, 'r') as handle:
                                for line in handle:
                                        paths = line.split()
                                        modalitiesPaths = {}
                                        modalitiesPaths['T1'] = paths[0]
                                        modalitiesPaths['T1C'] = paths[1]
                                        modalitiesPaths['T2'] = paths[2]
                                        modalitiesPaths['FLAIR'] = paths[3]
                                        labelPath = paths[4]
                                        patientFilePaths = (modalitiesPaths, labelPath)
                                        patientsFilePaths[fileName + '_' + str(i)] = patientFilePaths
                                        i = i + 1
                                patientsFilePathsByType[fileName] = patientsFilePaths
                return patientsFilePathsByType

        #Iterate over all training patient IDs for training batches generation
        def getPatientObjectForTrainingBatchesGeneration(self):
                datasetFile = self.datasetFileToIndexMapping[self.currentTrainTypeIndex]
                self.currentTrainTypeIndex = self.currentTrainTypeIndex + 1
                self.currentTrainTypeIndex = self.currentTrainTypeIndex % len(self.datasetFileToIndexMapping)
                if datasetFile not in self.trainingPatientIDs:
                        return None
                patientIndex = self.trainingPatientIDs[datasetFile][self.currentTrainPatientIndices[datasetFile]]
                self.currentTrainPatientIndices[datasetFile] = self.currentTrainPatientIndices[datasetFile] + 1
                self.currentTrainPatientIndices[datasetFile] = self.currentTrainPatientIndices[datasetFile] % len(self.trainingPatientIDs[datasetFile])
                patientData = InputGenerator.patientDataObjectManager.getPatientDataObject(datasetFile, patientIndex)
                return patientData

        #Iterate over all test patient IDs for test(validation) batches generation
        def getPatientObjectForTestBatchesGeneration(self):
                datasetFile = self.datasetFileToIndexMapping[self.currentTestTypeIndex]
                self.currentTestTypeIndex = self.currentTestTypeIndex + 1
                self.currentTestTypeIndex = self.currentTestTypeIndex % len(self.datasetFileToIndexMapping)
                if datasetFile not in self.testPatientIDs:
                        return None
                patientIndex = self.testPatientIDs[datasetFile][self.currentTestPatientIndices[datasetFile]]
                self.currentTestPatientIndices[datasetFile] = self.currentTestPatientIndices[datasetFile] + 1
                self.currentTestPatientIndices[datasetFile] = self.currentTestPatientIndices[datasetFile] % len(self.testPatientIDs[datasetFile])
                patientData = InputGenerator.patientDataObjectManager.getPatientDataObject(datasetFile, patientIndex)
                return patientData

        #Method to get next few training batches
        #Arguments:
        #1. list of patch sizes
        #2. Number of input patches per label
        #3. List of per input shapes. Each per input shape is itself a list.
        #4. Number of training batches to be generated per patient.
        #5. threeDPatches: True if patches are to be generated in three planes.
        #6. normalized: If true, patches are generated from normalized image array.
        #Output: List of batches to be processed by the learning model
        def getNextTrainingBatches(self, patchSizes, batchSizePerLabel, perInputShapes, numBatches=1, threeDPatches=False, normalized=True, balanced=True):
                patientData = self.getPatientObjectForTrainingBatchesGeneration()
                if patientData is None:
                        return None
                batches = []
                for numBatch in range(numBatches):
                        if balanced:
                                batch = patientData.getBalancedBatch(patchSizes, batchSizePerLabel, perInputShapes, threeDPatches, normalized)
                        else:
                                batch = patientData.getUnbalancedBatch(patchSizes, batchSizePerLabel, perInputShapes, threeDPatches, normalized)
                        batches.append(batch)
                return batches

        #Similar to getNextTrainingBatches method.
        def getNextTestBatches(self, patchSizes, batchSizePerLabel, perInputShapes, numBatches=1, threeDPatches=False, normalized=True, balanced=True):
                patientData = self.getPatientObjectForTestBatchesGeneration()
                if patientData is None:
                        return None
                batches = []
                for numBatch in range(numBatches):
                        if balanced:
                                batch = patientData.getBalancedBatch(patchSizes, batchSizePerLabel, perInputShapes, threeDPatches, normalized)
                        else:
                                batch = patientData.getUnbalancedBatch(patchSizes, batchSizePerLabel, perInputShapes, threeDPatches, normalized)
                        batches.append(batch)
                return batches
