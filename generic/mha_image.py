import numpy as np
import SimpleITK
import sys, io
import os
#import h5py
from threading import Thread
import math


class MhaImage:

	#initialize an MHA image object, imageFilePath can be none, patientID and modalities are strings
	def __init__(self, patientID, modality, imageFilePath):
		self.patientID = patientID
		self.modality = modality
		self.imageFilePath = imageFilePath

	#Get Array from imageFilePath specified in the constructor. Execute curvature flow if denoise is true. Returns float image array
	def getArrayFromFile(self, denoise=None, n4BiasFieldCorrection=None):
		image = SimpleITK.ReadImage(self.imageFilePath, SimpleITK.sitkFloat32)
		if denoise is not None and denoise[self.modality]:
			image = SimpleITK.CurvatureFlow(image1=image, timeStep=0.125, numberOfIterations=5)
		#Apply N4 Bias field correction for T1 and T1C scans
		if n4BiasFieldCorrection is not None and n4BiasFieldCorrection[self.modality]:
			print "starting bias field correction"
			maskImage = SimpleITK.OtsuThreshold(image, 0, 1, 200)
			corrector = SimpleITK.N4BiasFieldCorrectionImageFilter();
			image = corrector.Execute(image, maskImage)
			print "completed bias field correction"
		self.completeImageArray = SimpleITK.GetArrayFromImage(image)

	#Create MHA file into specified outputPath form specified image array
	def writeMhaFileFromArray(self, outputFilePath, imageArray):
		image = SimpleITK.GetImageFromArray(imageArray)
		SimpleITK.WriteImage(image, outputFilePath)

	#Execute linear or non linear normalization after standard normalization. TODO:Choose normalizationType using normalizationParams
	def getNormalizedImageArray(self, normalizationParams):
		self.performStandardNormalization()
		self.performLinearScaling(normalizationParams)

	#Save normalized image array to file. Not required. TODO: execution requires installation of h5py library
	def saveNormalizedImageArrayToFile(self, fileName):
		try:
			dirName = 'normalizedDataset/normalizedPatientData_' + str(self.patientID)
			if not os.path.exists(dirName):
				os.makedirs(dirName)
			#h5f = h5py.File(fileName, 'w')
			#h5f.create_dataset('dataset', data=self.normalizedArray)
			#h5f.close()
		except:
			print "error writing normalized array"

	#Method to perform standard normalization (Subtract each element by mean and divide by standard deviation)
	def performStandardNormalization(self):
		self.normalizedArray = self.completeImageArray * 1.0
		meanValue = float(np.mean(self.normalizedArray)) * 1.0
		standardDeviation = float(np.std(self.normalizedArray)) * 1.0
		self.normalizedArray = (self.normalizedArray - meanValue) / standardDeviation
		self.zeroEquivalent = float(self.normalizedArray.min())

	#Method to perform linear normalization
	def performLinearScaling(self, normalizationParams):
		try:
			minValue = float(self.normalizedArray.min())
			maxValue = float(self.normalizedArray.max())
			self.normalizedArray = ((self.normalizedArray - minValue) * ((normalizationParams['newMax'] - normalizationParams['newMin']) / (maxValue - minValue))) + normalizationParams['newMin']
			self.zeroEquivalent = float(self.normalizedArray.min())
		except:
			print "error normalizing the image, normalizationParams incorrect"

	#Method to perform non-linear normalization of the image. Change method name in getNormalizedImageArray() to take affect
	def performNonLinearScaling(self, normalizationParams):
		try:
			minValue = float(self.normalizedArray.min())
			maxValue = float(self.normalizedArray.max())
			self.normalizedArray = ((normalizationParams['newMax'] - normalizationParams['newMin']) / (1 + math.exp(-(self.normalizedArray - normalizationParams['beta']) / normalizationParams['alpha']))) + normalizationParams['newMin']
			self.zeroEquivalent = float(self.normalizedArray.min())
		except:
			print "error normalizing the image, normalizationParams incorrect"

	#DEPRECATED METHOD - Not used any more. Takes patch of size patchSize around z,x,y in xy plane
	def getPatchAroundCoordinate(self, z, x, y, patchSize):
		max_x = self.completeImageArray.shape[1]
		max_y = self.completeImageArray.shape[2]
		delta = patchSize / 2
		appended_image = np.zeros((max_x + patchSize, max_y + patchSize), dtype=self.completeImageArray.dtype)
		appended_image[delta:max_x+delta, delta:max_y+delta] = self.completeImageArray[z, 0:max_x, 0:max_y]
		patch = np.copy(appended_image[x:x+patchSize,y:y+patchSize])
		return patch

	#Returns patch in xy plane. size of patch (patchSize * patchSize). Normalized patch if normalized = true.
	def getXYPatchAroundCoordinate(self, z, x, y, patchSize, normalized=True):
		if normalized:
			imgArray = self.normalizedArray
		else:
			imgArray = self.completeImageArray
		max_x = imgArray.shape[1]
		max_y = imgArray.shape[2]
		delta = patchSize / 2
		appended_image = np.zeros((max_x + patchSize, max_y + patchSize), dtype=np.float32)
		appended_image[:,:] = self.zeroEquivalent
		appended_image[delta:max_x+delta, delta:max_y+delta] = imgArray[z, 0:max_x, 0:max_y]
		xyPatch = np.copy(appended_image[x:x+patchSize,y:y+patchSize])
		return xyPatch

	#Returns patch in xz plane
	def getZXPatchAroundCoordinate(self, z, x, y, patchSize, normalized=True):
		if normalized:
			imgArray = self.normalizedArray
		else:
			imgArray = self.completeImageArray
		max_x = imgArray.shape[1]
		max_z = imgArray.shape[0]
		delta = patchSize / 2
		appended_image = np.zeros((max_z + patchSize, max_x + patchSize), dtype=np.float32)
		appended_image[:,:] = self.zeroEquivalent
		appended_image[delta:max_z+delta, delta:max_x+delta] = imgArray[0:max_z, 0:max_x, y]
		zxPatch = np.copy(appended_image[z:z+patchSize,x:x+patchSize])
		return zxPatch

	#Returns patch in yz plane
	def getZYPatchAroundCoordinate(self, z, x, y, patchSize, normalized=True):
		if normalized:
			imgArray = self.normalizedArray
		else:
			imgArray = self.completeImageArray
		max_z = imgArray.shape[0]
		max_y = imgArray.shape[2]
		delta = patchSize / 2
		appended_image = np.zeros((max_z + patchSize, max_y + patchSize), dtype=np.float32)
		appended_image[:,:] = self.zeroEquivalent
		appended_image[delta:max_z+delta, delta:max_y+delta] = self.normalizedArray[0:max_z, x, 0:max_y]
		zyPatch = np.copy(appended_image[z:z+patchSize,y:y+patchSize])
		return zyPatch

	#Get 3 different patches in 3 planes; xy, xz and yz
	def getThreeDimensionalPatch(self, z, x, y, patchSize, normalized=True):
		xyPatch = self.getXYPatchAroundCoordinate(z, x, y, patchSize, normalized)
		zxPatch = self.getZXPatchAroundCoordinate(z, x, y, patchSize, normalized)
		zyPatch = self.getZYPatchAroundCoordinate(z, x, y, patchSize, normalized)
		return (xyPatch, zxPatch, zyPatch)
