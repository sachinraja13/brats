import sys, io
import numpy as np
import os
from threading import Thread
import Queue
import time

class MultiThreadedPatientPredictionBatchGenerator:

        #Initialize MultiThreadedPatientPredictionBatchGenerator object
        #Arguments:
        #1. patient data object
        #2. size of each prediction batch
        #3. parameter dictionary for prediction batches generation
        #4. number of prediction batches generating threads
        #5. maximum coordinates queue size. Coordinates from this queue are used for batches generation for prediction
        #6. maximum prediction batches queue size.
        def __init__(self, patientData, predictionBatchSize, predictionBatchGenerationParams ,numPredictionBatchesGeneratingThreads, predictionCoordinatesQueueMaxSize, predictionBatchesQueueMaxsize):
                self.patientData = patientData
                self.patientData.predX = 0
                self.patientData.predY = 0
                self.patientData.predZ = 0
                self.patientData.predictedImage = np.zeros(self.patientData.imageShape, dtype=self.patientData.labelData.completeImageArray.dtype)
                self.predictionBatchGenerationParams = predictionBatchGenerationParams
                self.predictionBatchSize = predictionBatchSize
                self.predictionBatchGeneratorExitSignal = False
                self.predictionCoordinatesQueue = Queue.Queue(maxsize=predictionCoordinatesQueueMaxSize)
                self.predictionBatchesQueue = Queue.Queue(maxsize=predictionBatchesQueueMaxsize)
                self.predictionCoordinatesGeneratingThread = Thread(target=self.getCoordinatesForNextPredictionBatch, args=())
                self.predictionCoordinatesGeneratingThread.start()
                self.predictionBatchesGeneratingThreads = {}
                for i in range(numPredictionBatchesGeneratingThreads):
                        thread = Thread(target=self.addNextPredictionBatchesToQueue, args=())
                        thread.start()
                        self.predictionBatchesGeneratingThreads[i] = thread

        #Find coordinates for prediction. Add to coordinates queue from which predictionBatchesGeneratingThreads read coordinates to generate input patches
        def getCoordinatesForNextPredictionBatch(self):
                while True:
                        batchIndex = 0
                        coordinates = []
                        while True:
                                #execute loop till size of coordinates list is equal to batch size or till the last coordinate of the brain processed sequentially
                                if batchIndex >= self.predictionBatchSize:
                                        break
                                self.patientData.getNextCoordinate()
                                coordinate = (self.patientData.predZ, self.patientData.predX, self.patientData.predY)
                                if self.patientData.predZ is None or self.patientData.predX is None or self.patientData.predY is None:
                                        coordinate = None
                                if coordinate is None:
                                        break
                                self.patientData.areAllModalitiesPixelsZero(coordinate[0], coordinate[1], coordinate[2])
                                #coordinate is added to the coordinates list which is added to coordinates queue only if one of the pixel values at that coor is non zero
                                if self.patientData.allModalitiesPixelsZeroflag == False:
                                        coordinates.append(coordinate)
                                        batchIndex = batchIndex + 1
                        #exit when no coordinate is found in the coordinates list
                        #None is the last element of the coordinates queue. Specifies that all the coordinates for prediction have already been added to the queue.
                        if len(coordinates) == 0:
                                self.predictionCoordinatesQueue.put(None)
                                print "All coordinates processed, exiting"
                                break
                        else:
                                self.predictionCoordinatesQueue.put(coordinates)

        #Reads coordinates from coordinates queue and generates input batches for prediction based on predictionBatchGenerationParams
        #Multiple threads can be created to execute this function to optimize batch generation speed
        def addNextPredictionBatchesToQueue(self):
                while True:
                        try:
                                coordinates = self.predictionCoordinatesQueue.get(False)
                                if coordinates is None:
                                        #if all elements of coordinates queue are processed, send exit signal to all other predictionBatchesGeneratingThreads
                                        self.predictionBatchGeneratorExitSignal = True
                                        break
                                else:
                                        self.addNextPredictionBatchToQueue(coordinates)
                        except Queue.Empty:
                                if self.predictionBatchGeneratorExitSignal:
                                        break
                                else:
                                        #print "predictionCoordinatesQueue is empty and exit sigal is false"
                                        time.sleep(1)
                                        continue

        #Takes in a list of coordinates as an argument, generates batch and adds it to predictionBatchesQueue using predictionBatchGenerationParams
        def addNextPredictionBatchToQueue(self, coordinates):
                patchSizes = self.predictionBatchGenerationParams['patchSizes']
                perInputShapes = self.predictionBatchGenerationParams['perInputShapes']
                threeDPatches = self.predictionBatchGenerationParams['threeDPatches']
                normalized = self.predictionBatchGenerationParams['normalized']
                xs = []
                coordinatesList = []
                #if threeDPatches is True, three patches are added per coordinate(one in each xy, xz and yz planes) and max vote is used for prediction
                if threeDPatches:
                        for coordinate in coordinates:
                                coordinatesList.extend([coordinate] * 3)
                else:
                        for coordinate in coordinates:
                                coordinatesList.extend([coordinate] * 1)

                #Generate multiple patches per coordinate if cascaded architecture is used.
                for i in range(len(patchSizes)):
                        patchSize = patchSizes[i]
                        perInputShape = perInputShapes[i]
                        perInputShape = list(perInputShape)
                        x = []
                        coordinateIndex = 0
                        for coordinate in coordinates:
                                if threeDPatches:
                                        patches = self.patientData.getThreeDimensionalPatch(coordinate[0], coordinate[1], coordinate[2], patchSize, normalized)
                                        for planeID in range(3):
                                                patch = patches[planeID]
                                                patch = patch.reshape(perInputShape)
                                                x.append(patch)
                                else:
                                        patch = self.patientData.getXYPatch(coordinate[0], coordinate[1], coordinate[2], patchSize, normalized)
                                        patch = patch.reshape(perInputShape)
                                        x.append(patch)
                                coordinateIndex = coordinateIndex + 1
                        lastCoordinate = coordinates[len(coordinates) - 1]

                        #if coordinate size is less than batch size, fill up batch by adding 0th coordinate. This is required for models with constant batch size.
                        while coordinateIndex < self.predictionBatchSize:
                                if threeDPatches:
                                        patches = self.patientData.getThreeDimensionalPatch(0, 0, 0, patchSize, normalized)
                                        for planeID in range(3):
                                                if i == 0:
                                                        coordinatesList.append(None)
                                                patch = patches[planeID]
                                                patch = patch.reshape(perInputShape)
                                                x.append(patch)
                                else:
                                        if i == 0:
                                                coordinatesList.append(None)
                                        patch = self.patientData.getXYPatch(0, 0, 0, patchSize, normalized)
                                        patch = patch.reshape(perInputShape)
                                        x.append(patch)
                                coordinateIndex = coordinateIndex + 1

                        xs.append(x)

                self.predictionBatchesQueue.put((coordinatesList, xs))
                print("Batch generated uptill : (" + str(lastCoordinate[0]) + ", " + str(lastCoordinate[1]) + ", " + str(lastCoordinate[2]) + ")")


        #Returns True if all the prediction batches generating threads are dead. Returns False otherwise
        def areAllPredictionBatchesGeneratingThreadsDead(self):
                flag = True
                for threadID in self.predictionBatchesGeneratingThreads:
                        thread = self.predictionBatchesGeneratingThreads[threadID]
                        if thread.is_alive():
                                flag = False
                return flag

        #Fetches a prediction batch from the predictionBatchesQueue. Returns None when the queue becomes empty and all predictionBatchesGeneratingThreads are dead
        def getNextPredictionBatch(self, retryInterval):
                while True:
                        try:
                                batch = self.predictionBatchesQueue.get(False)
                                return batch
                        except Queue.Empty:
                                if self.areAllPredictionBatchesGeneratingThreadsDead():
                                        print "all pred batcg generating threads are dead"
                                        return None
                                else:
                                        print("queue empty, will retry in " + str(retryInterval) + " seconds")
                                        time.sleep(retryInterval)
                                        continue
