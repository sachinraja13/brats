import sys, io
import numpy as np
import SimpleITK
import matplotlib.pyplot as plt
from crf_denoising.crf_image_denoiser_3d import CRFImageDenoiser3D
from crf_denoising.crf_image_denoiser_2d import CRFImageDenoiser2D

class ProjectUtil:

	#Write code for difference between 2 predicted Images
	def __init__(self):
		pass

	#Compares predictedImageArray with labelImageArray in terms of accuracy
	def comparePredictedImageWithTrueImageAccuracy(self, labelImageArray, predictedImageArray):
		print "comparing images in terms of accuracy"
		totalPixels = 0
		correctlyClassifiedPixels = 0
		imageShape = labelImageArray.shape
		for z in range(imageShape[0]):
			for x in range(imageShape[1]):
				for y in range(imageShape[2]):
					if labelImageArray[z,x,y] == predictedImageArray[z,x,y]:
						correctlyClassifiedPixels = correctlyClassifiedPixels + 1
					totalPixels = totalPixels + 1
		accuracy = float(correctlyClassifiedPixels) / float(totalPixels)
		print("\nnumber of correctly classified pixels: " + str(correctlyClassifiedPixels))
		print("number of total pixels: " + str(totalPixels))
		print("accuracy: " + str(accuracy) + "\n\n")

	#Compares predicted image array with labelImageArray in terms of F-Scores
	def comparePredictedImageWithTrueImage(self, labelImageArray, predictedImageArray, uniqueLabels=5, predictionPatientID='Not Specified'):
		print "comparing images in terms of F Scores"
		imageShape = labelImageArray.shape
		totalPixels = imageShape[0] * imageShape[1] * imageShape[2]
		labelImageArray = labelImageArray.reshape(totalPixels)
		predictedImageArray = predictedImageArray.reshape(totalPixels)
		print "reshaping done"
		labelImageArray = labelImageArray.tolist()
		predictedImageArray = predictedImageArray.tolist()
		print "list preparation done"
		results = self.getFScoresResults(labelImageArray, predictedImageArray, 5)
		self.printResults(results)

	#Compares F-Scores and other evaluation metrics using true labels array and predicted labels array.
	def getFScoresResults(self, trueLabels, predictedLabels, numberOfLabels=5):
		finalResults = {}
		diceScores = {}
		fScores = {}
		trueActive = 0
		predictedActive = 0
		trueAndPredictedActive = 0
		trueCore = 0
		predictedCore = 0
		trueAndPredictedCore = 0
		trueWhole = 0
		predictedWhole = 0
		trueAndPredictedWhole = 0
		for j in range(len(trueLabels)):
			orig = trueLabels[j]
			pred = predictedLabels[j]
			if orig == 1 or orig == 2 or orig == 3 or orig == 4:
				trueWhole = trueWhole + 1
				if pred == 1 or pred == 2 or pred == 3 or pred == 4:
					trueAndPredictedWhole = trueAndPredictedWhole + 1
			if pred == 1 or pred == 2 or pred == 3 or pred == 4:
				predictedWhole = predictedWhole + 1
			if orig == 1 or orig == 3 or orig == 4:
				trueCore = trueCore + 1
				if pred == 1 or pred == 3 or pred == 4:
					trueAndPredictedCore = trueAndPredictedCore + 1
			if pred == 1 or pred == 3 or pred == 4:
				predictedCore = predictedCore + 1
			if orig == 4:
				trueActive = trueActive + 1
				if pred == 4:
					trueAndPredictedActive = trueAndPredictedActive + 1
			if pred == 4:
				predictedActive = predictedActive + 1


		for i in range(numberOfLabels):
			result = {}
			precision = 0
			recall = 0
			correctlyClassifiedAsI = 0
			labelMisclassifiedAs = {}
			classifiedAsI = 0
			trueI = 0
			for j in range(len(trueLabels)):
				orig = trueLabels[j]
				pred = predictedLabels[j]
				if orig == i and pred == i:
					correctlyClassifiedAsI = correctlyClassifiedAsI + 1
				if orig == i and pred != i:
					misclassifiedAs = pred
					if misclassifiedAs not in labelMisclassifiedAs:
						labelMisclassifiedAs[misclassifiedAs] = 0
					labelMisclassifiedAs[misclassifiedAs] = labelMisclassifiedAs[misclassifiedAs] + 1
				if orig == i:
					trueI = trueI + 1
				if pred == i:
					classifiedAsI = classifiedAsI + 1
			try:
				precision = float(correctlyClassifiedAsI) / float(classifiedAsI)
			except:
				precision = 0.0
			try:
				recall = float(correctlyClassifiedAsI) / float(trueI)
			except:
				recall = 0.0
			try:
				f_score = (2 * precision * recall) / (precision + recall)
			except:
				f_score = 0.0
			try:
				dice_score = float(2 * correctlyClassifiedAsI) / (float(trueI) + float(classifiedAsI))
			except:
				dice_score = 0.0

			result['totalPixels'] = len(trueLabels)
			result['precision'] = precision
			result['recall'] = recall
			result['F-Score'] = f_score
			result['Dice Score'] = dice_score
			result['trueI'] = trueI
			result['correctlyClassifiedAsI'] = correctlyClassifiedAsI
			result['classifiedAsI'] = classifiedAsI
			result['labelMisclassifiedAs'] = labelMisclassifiedAs
			result['percentageOfTruePixels'] = (float(trueI) / float(len(trueLabels))) * 100.0
			result['percentageOfCorrectlyClassifiedPixels'] = (float(correctlyClassifiedAsI) / float(len(trueLabels))) * 100.0
			result['percentageOfClassifiedPixels'] = (float(classifiedAsI) / float(len(trueLabels))) * 100.0
			fScores['Label ' + str(i)] = f_score
			diceScores['Label ' + str(i)] = dice_score
			finalResults[i] = result

		try:
			dice_score_whole = float(2 * trueAndPredictedWhole) / (float(trueWhole) + float(predictedWhole))
		except:
			dice_score_whole = 0.0
		try:
			precision = float(trueAndPredictedWhole) / float(predictedWhole)
			recall = float(trueAndPredictedWhole) / float(trueWhole)
			f_score_whole = (2 * precision * recall) / (precision + recall)
		except:
			f_score_whole = 0.0
		fScores['Whole Tumor Region'] = f_score_whole
		diceScores['Whole Tumor Region'] = dice_score_whole

		try:
			dice_score_core = float(2 * trueAndPredictedCore) / (float(trueCore) + float(predictedCore))
		except:
			dice_score_core = 0.0
		try:
			precision = float(trueAndPredictedCore) / float(predictedCore)
			recall = float(trueAndPredictedCore) / float(trueCore)
			f_score_core = (2 * precision * recall) / (precision + recall)
		except:
			f_score_core = 0.0
		fScores['Core Tumor Region'] = f_score_core
		diceScores['Core Tumor Region'] = dice_score_core

		try:
			dice_score_active = float(2 * trueAndPredictedActive) / (float(trueActive) + float(predictedActive))
		except:
			dice_score_active = 0.0
		try:
			precision = float(trueAndPredictedActive) / float(predictedActive)
			recall = float(trueAndPredictedActive) / float(trueActive)
			f_score_active = (2 * precision * recall) / (precision + recall)
		except:
			f_score_active = 0.0
		fScores['Active Tumor Region'] = f_score_active
		diceScores['Active Tumor Region'] = dice_score_active

		finalResults['F-Scores'] = fScores
		finalResults['Dice-Scores'] = diceScores


		return finalResults

	#Print the results dictionary obtained form getFScoresResults.
	def printResults(self, results):
		print("\nF-Score Results...")
		for label in results:
			labelDict = results[label]
			print("\tLabel : " + str(label))
			for key in labelDict:
				if type(labelDict[key]) is not dict:
					print("\t\t" + str(key) + " : " + str(labelDict[key]))
				else:
					print("\t\t" + str(key) + " : " + str(sum(labelDict[key].values())))
					for nestedKey in labelDict[key]:
						print("\t\t\t" + str(nestedKey) + " : " + str(labelDict[key][nestedKey]))
		print("\n")

	def printResultsInLatexTabularFormat(self, finalResults):
		print("\n\nLatex Format\n")
		print("Result Type & Normal Tissue & Necrosis & Edema & Non-enhancing & Enhancing & Whole & Core & Active")
		print("Dice Scores & " + str(finalResults['Dice-Scores']['Label 0']) + " & " + str(finalResults['Dice-Scores']['Label 1']) + " & " + str(finalResults['Dice-Scores']['Label 2']) + " & " + str(finalResults['Dice-Scores']['Label 3']) + " & " + str(finalResults['Dice-Scores']['Label 4']) + " & " + str(finalResults['Dice-Scores']['Whole Tumor Region']) + " & " + str(finalResults['Dice-Scores']['Core Tumor Region']) + " & " + str(finalResults['Dice-Scores']['Active Tumor Region']))
		print("F Scores & " + str(finalResults['F-Scores']['Label 0']) + " & " + str(finalResults['F-Scores']['Label 1']) + " & " + str(finalResults['F-Scores']['Label 2']) + " & " + str(finalResults['F-Scores']['Label 3']) + " & " + str(finalResults['F-Scores']['Label 4']) + " & " + str(finalResults['F-Scores']['Whole Tumor Region']) + " & " + str(finalResults['F-Scores']['Core Tumor Region']) + " & " + str(finalResults['F-Scores']['Active Tumor Region'])) 

	#Plot precision-recall curves. Method not used now. TODO: Use method
	def plotPrecisionRecallCurves(self, precisions, recalls, predictionPatientID, uniqueLabels=5):
		graphTitle = 'Precision-Recall curve'
		for i in range(uniqueLabels):
			precision = precisions[i]
			recall = recalls[i]
			graphLabel = "Precision-Recall curve for label : " + str(label)
			plt.plot(recall, precision, label=graphLabel)
		plt.xlim([0.0, 1.0])
		plt.ylim([0.0, 1.05])
		plt.xlabel('Recall')
		plt.ylabel('Precision')
		plt.title(graphTitle)
		plt.legend(loc="lower right")
		plt.show()

	#Generate MHA image using image array in the specified output file path
	def generateMhaImageFromImageArray(self, imageArray, outputFilePath):
		image = SimpleITK.GetImageFromArray(imageArray)
		SimpleITK.WriteImage(image, outputFilePath)

	#Creates an overlay of overlayOfMha image on overlayOnMha image for better visualizations.
	def generateOverlayImage(self, overlayOnMhaFilePath, overlayOfMhaFilePath, outputFilePath, labelContour=True):
		overlayOnMhaImage = SimpleITK.ReadImage(overlayOnMhaFilePath)
		overlayOfMhaImage = SimpleITK.ReadImage(overlayOfMhaFilePath)
		#overlayOfMhaImage = SimpleITK.LabelToRGB(overlayOfMhaImage)
		overlayOfMhaImage.CopyInformation(overlayOnMhaImage)
		overlayOnMhaImage = SimpleITK.Cast(SimpleITK.RescaleIntensity(overlayOnMhaImage), SimpleITK.sitkUInt8)
		if labelContour:
			overlayedImage = SimpleITK.LabelOverlay(overlayOnMhaImage, SimpleITK.LabelContour(overlayOfMhaImage), opacity=0.5)
		else:
			overlayedImage = SimpleITK.LabelOverlay(overlayOnMhaImage, overlayOfMhaImage, opacity=0.25)
		overlayedImageArray = SimpleITK.GetArrayFromImage(overlayedImage)
		self.generateMhaImageFromImageArray(overlayedImageArray, outputFilePath)

	def postProcessPredictionUsingCRFDenoising2D(self, noisyImageArray):
		denoisedArray = np.zeros(noisyImageArray.shape)
		for z in range(noisyImageArray.shape[0]):
			noisyArray = noisyImageArray[z,:,:]
			crfImageDenoiser2D = CRFImageDenoiser2D(noisyArray)
			if not np.any(noisyArray):
				continue
			denoisedArray[z,:,:] = crfImageDenoiser2D.executeCRF()
			del(crfImageDenoiser2D)
		return denoisedArray
